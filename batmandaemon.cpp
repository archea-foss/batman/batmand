/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "batmandaemon.h"

#include <sys/socket.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>

#include <QCommandLineParser>
#include <QCoreApplication>
#include <QSocketNotifier>

#include <metamodel/metameasurements.h>
#include <modbusproxy/modbustcpclient.h>
#include <sampler/sampler.h>

#include <QtDebug>

int BatManDaemon::sighupFd[2];
int BatManDaemon::sigtermFd[2];
int BatManDaemon::sigUser1Fd[2];

BatManDaemon::BatManDaemon(QObject *parent)
    : QObject{parent}
    , mThreadsToSyncBeforeQuit(0)
{
    connect(this, &BatManDaemon::sigTerm, this, &BatManDaemon::quit);

}

BatManDaemon::~BatManDaemon()
{
    if (mSampler)
        delete mSampler;

    if (mSamplerThread)
        delete mSamplerThread;

    if (mMeasurements)
        delete mMeasurements;

    if (mDataCache)
        delete mDataCache;

}


void BatManDaemon::setup()
{

    // Setup signals handlers for Unix signals
    if (::socketpair(AF_UNIX, SOCK_STREAM, 0, sighupFd))
       qFatal("Couldn't create HUP socketpair");

    // fcntl(sighupFd[1], F_SETFL, O_NONBLOCK);

    if (::socketpair(AF_UNIX, SOCK_STREAM, 0, sigtermFd))
       qFatal("Couldn't create TERM socketpair");

    if (::socketpair(AF_UNIX, SOCK_STREAM, 0, sigUser1Fd))
       qFatal("Couldn't create USR1 socketpair");

    // fcntl(sigtermFd[1], F_SETFL, O_NONBLOCK);

    snHup = new QSocketNotifier(sighupFd[1], QSocketNotifier::Read, this);
    connect(snHup, &QSocketNotifier::activated, this, &BatManDaemon::handleSigHup);

    snTerm = new QSocketNotifier(sigtermFd[1], QSocketNotifier::Read, this);
    connect(snTerm, &QSocketNotifier::activated, this, &BatManDaemon::handleSigTerm);

    snUser1 = new QSocketNotifier(sigUser1Fd[1], QSocketNotifier::Read, this);
    connect(snUser1, &QSocketNotifier::activated, this, &BatManDaemon::handleSigUser1);


    setup_unix_signal_handlers();


    if (mSettings.measurementConfig().toString().isEmpty())
    {
        qFatal() << "Failed to load monitorconfig. Terminating.";
    }

    if (mSettings.dataPath().toString().isEmpty())
    {
        qFatal() << "No path is set for the data logger. Terminating.";
    }


    mModbusProxyThread = new QThread(this);
    mModbusProxy = new ModbusProxy(mSettings.studerHostname(), mSettings.studerPort());
    mModbusProxy->moveToThread(mModbusProxyThread);

    connect(mModbusProxyThread.data(), &QThread::started, mModbusProxy.data(), &ModbusProxy::start);
    connect(mModbusProxy.data(), &ModbusProxy::finished, mModbusProxyThread.data(), &QThread::quit);
    connect(mModbusProxyThread.data(), &QThread::finished, this, &BatManDaemon::syncBeforeQuit);
    connect(this, &BatManDaemon::shutdown, mModbusProxy.data(), &ModbusProxy::quit);


    mMeasurements = new MetaMeasurements(mSettings.measurementConfig().toString());
    // qDebug().noquote() << "Measurements: " << Qt::endl << mMeasurements->toString("   ");

    mSamplerThread = new QThread(this);

    mSampler = new Sampler(mMeasurements, mModbusProxy);
    mSampler->moveToThread(mSamplerThread);

    connect(mSamplerThread, &QThread::started, mSampler, &Sampler::start);
    connect(mSampler.data(), &Sampler::finished, mSamplerThread.data(), &QThread::quit);
    connect(mSamplerThread.data(), &QThread::finished, this, &BatManDaemon::syncBeforeQuit);
    connect(this, &BatManDaemon::shutdown, mSampler.data(), &Sampler::quit);

    mManagerThread = new QThread(this);
    mManager = new Manager(mModbusProxy, mSettings.managerConfig().toString());
    mManager->moveToThread(mManagerThread);

    connect(mManagerThread.data(), &QThread::started, mManager.data(), &Manager::start);
    connect(mManager.data(), &Manager::finished, mManagerThread.data(), &QThread::quit);
    connect(this, &BatManDaemon::shutdown, mManager.data(), &Manager::quit);
    connect(mManagerThread.data(), &QThread::finished, this, &BatManDaemon::syncBeforeQuit);

    mLoggerThread = new QThread(this);
    mLogger = new DataLogger(mMeasurements, mSettings.dataPath().toString());
    mLogger->moveToThread(mLoggerThread);
    connect(mSampler.data(), &Sampler::readData, mLogger.data(), &DataLogger::loggDataset);

    connect(mLoggerThread, &QThread::started, mLogger.data(), &DataLogger::start);
    connect(mLogger.data(), &DataLogger::finished, mLoggerThread.data(), &QThread::quit);
    connect(this, &BatManDaemon::shutdown, mLogger.data(), &DataLogger::quit);
    connect(mLoggerThread.data(), &QThread::finished, this, &BatManDaemon::syncBeforeQuit);

    mDataCache = new DataCache(mMeasurements->memoryRetention(), this);
    mDataCache->setup();

    connect(mSampler.data(), &Sampler::readData, mDataCache.data(), &DataCache::cacheDataset);

    if (mMeasurements->pubSubHost().value("Enabled").toBool() == true &&
        mMeasurements->pubSubHost().value("Port").canConvert<int>() == true)
    {
        mMeasurementsHostThread = new QThread();
        mMeasurementsHost = new PubSubHost(mMeasurements->pubSubHost().value("Port").toInt());

        mMeasurementsHost->moveToThread(mMeasurementsHostThread);

        connect(mMeasurementsHostThread.data(), &QThread::started, mMeasurementsHost.data(), &PubSubHost::start);
        connect(this, &BatManDaemon::shutdown, mMeasurementsHost.data(), &PubSubHost::quit);
        connect(mMeasurementsHost.data(), &PubSubHost::finished, mMeasurementsHostThread.data(), &QThread::quit);

        connect(mSampler.data(), &Sampler::readData, mMeasurementsHost.data(), &PubSubHost::publish);
        connect(mMeasurementsHostThread.data(), &QThread::finished, this, &BatManDaemon::syncBeforeQuit);

    }

}

void BatManDaemon::start()
{
    mModbusProxyThread->start();
    mSamplerThread->start();
    mLoggerThread->start();
    mManagerThread->start();

    mThreadsToSyncBeforeQuit = 4;

    if (mMeasurementsHostThread)
    {
        mMeasurementsHostThread->start();
        mThreadsToSyncBeforeQuit++;
    }

}

void BatManDaemon::quit()
{

    // Timeout for synchronizing threads
    QTimer *timer = new QTimer(this);
    timer->singleShot(5000, QCoreApplication::instance(), &QCoreApplication::quit);

    qDebug() << "Daemon quit called, shuting down";

    // Terminate the threads throw a signal so that it is processed in it's own thread.
    emit shutdown();


}


void BatManDaemon::hupSignalHandler(int unused)
{
    Q_UNUSED(unused)

    char a = 1;
    ::write(sighupFd[0], &a, sizeof(a));
}

void BatManDaemon::termSignalHandler(int unused)
{
    Q_UNUSED(unused)

    char a = 1;
    ::write(sigtermFd[0], &a, sizeof(a));
}

void BatManDaemon::user1SignalHandler(int unused)
{
    Q_UNUSED(unused)

    char a = 1;
    ::write(sigUser1Fd[0], &a, sizeof(a));

}

void BatManDaemon::handleSigHup()
{

    snHup->setEnabled(false);

    char tmp;
    ::read(sighupFd[1], &tmp, sizeof(tmp));

    emit sigHup();

    snHup->setEnabled(true);
}

void BatManDaemon::handleSigTerm()
{
    // qDebug() << "Daemon SIGTERM called";

    snTerm->setEnabled(false);

    char tmp;
    ::read(sigtermFd[1], &tmp, sizeof(tmp));

    emit sigTerm();

    snTerm->setEnabled(true);

    // qDebug() << "Daemon SIGTERM handler done";
}

void BatManDaemon::handleSigUser1()
{

    snUser1->setEnabled(false);

    char tmp;
    ::read(sigUser1Fd[1], &tmp, sizeof(tmp));

    emit sigTerm();

    snUser1->setEnabled(true);

}

void BatManDaemon::syncBeforeQuit()
{
    mThreadsToSyncBeforeQuit--;

    if (mThreadsToSyncBeforeQuit <= 0)
        QCoreApplication::instance()->quit();

}

int BatManDaemon::setup_unix_signal_handlers()
{
    struct sigaction hup, term, usr1;

    hup.sa_handler = BatManDaemon::hupSignalHandler;
    sigemptyset(&hup.sa_mask);
    hup.sa_flags = 0;
    hup.sa_flags |= SA_RESTART;

    if (sigaction(SIGHUP, &hup, 0))
    {
        qWarning() << "Failed to setup SIGHUP";
        return 1;
    }

    term.sa_handler = BatManDaemon::termSignalHandler;
    sigemptyset(&term.sa_mask);
    term.sa_flags = 0;
    term.sa_flags |= SA_RESTART;

    if (sigaction(SIGTERM, &term, 0))
    {
        qWarning() << "Failed to setup SIGTERM";
        return 2;
    }

    usr1.sa_handler = BatManDaemon::user1SignalHandler;
    sigemptyset(&usr1.sa_mask);
    usr1.sa_flags = 0;
    usr1.sa_flags |= SA_RESTART;

    if (sigaction(SIGTERM, &usr1, 0))
    {
        qWarning() << "Failed to setup SIGUSR1";
        return 2;
    }
    return 0;

}

