/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef MODBUSPROXY_H
#define MODBUSPROXY_H

#include "modbustcpclient.h"
#include "modbusholdingregister.h"
#include "modbusholdingregisterset.h"

#include <QObject>
#include <QModbusDevice>
#include <QPointer>
#include <QTimer>
#include <QQueue>

class ModbusProxy : public QObject
{
        Q_OBJECT
    public:
        explicit ModbusProxy(const QVariant& hostname, const QVariant& port, QObject *parent = nullptr);
        virtual ~ModbusProxy();

        virtual bool event(QEvent *event) override;

        bool isConnected() const;

    public slots:
        void start();
        void quit();

        void send(ModbusHoldingRegister *request);
        void send(ModbusHoldingRegisterSet *set);

    protected slots:
        void modbusConnectionStatusChanged(QModbusDevice::State newState);
        void modbusConnect();
        void processQueue();

    protected:

    signals:

        void started();
        void finished();
        void connected();
        void lostConnection();


    private:
        bool                                        bIsConnected;
        bool                                        bAreProcessing;
        QVariant                                    mHostName;
        QVariant                                    mTcpPort;
        QPointer<ModbusTCPClient>             mModbusClient;
        QPointer<QTimer>                            mReconnectTimer;

        QQueue< QPointer<ModbusHoldingRegister> >   mRequestQueue;

        QMutex                                      mProcessingMutex;


};

#endif // MODBUSPROXY_H
