/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "valueapi.h"
#include <QHttpServer>
#include <QJsonDocument>

ValueAPI::ValueAPI(const QString &endpoint, QObject *parent)
    : QObject{parent}
    , mEndpoint {endpoint}
{

}

void ValueAPI::setup(const QString &prefix, QHttpServer *server)
{
    if (server)
    {
        qDebug() << "Adding route for " << prefix << "" << mEndpoint;

        server->route(QString("%1/%2/latest").arg(prefix).arg(mEndpoint),
                      [this](const QHttpServerRequest &request)
        {
            qDebug() << "GET " << request.method();
            return this->latestValue();
        });
    }

}

QString ValueAPI::latestValue() const
{
    QVariantMap   vmap;

    vmap.insert(mEndpoint, "Value of sort");

    return QString::fromUtf8(QJsonDocument::fromVariant(vmap).toJson());

}
