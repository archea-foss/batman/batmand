/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef PROFILESETTINGS_H
#define PROFILESETTINGS_H

#include "profilecommandsettings.h"

#include <QObject>

class ProfileSettings : public QObject
{
        Q_OBJECT

    public:
        static const QString         sName;
        static const QString         sColor;
        static const QString         sCommands;
        static const QString         sIsDefault;

    public:
        explicit ProfileSettings(QVariantMap map, QObject *parent = nullptr);
        QString toString(const QString& prefix = "", const QString &subprefix = "") const;
        QString name() const;
        QString color() const;
        bool isDefault() const;

        QList<ProfileCommandSettings> commands() const;

    signals:

    private:

        QString             mName;
        QString             mColor;
        bool                bIsDefault;

        QList<ProfileCommandSettings>      mCommands;



};

#endif // PROFILESETTINGS_H
