/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef MODBUSDATAUNIT_H
#define MODBUSDATAUNIT_H


#include <QObject>
#include<QModbusDataUnit>

class ModbusDataUnit : public QObject
{
        // Q_OBJECT

    public:

        enum DataType:int {None = 0, Bool, Int, UInt, Float, Float64, Enum, Bitfield, Invalid};
        static const QStringList sDataType;

    public:

        explicit ModbusDataUnit(int deviceAdress, int address, DataType type, QObject* parent = nullptr);
        explicit ModbusDataUnit(int deviceAdress, int address, const QString &type, QObject* parent = nullptr);
        explicit ModbusDataUnit(const ModbusDataUnit& other);

        virtual ~ModbusDataUnit();

        ModbusDataUnit* clone(QObject* parent = nullptr);

        QModbusDataUnit &unit();

        QModbusDataUnit* createUnit() const;

        static int typeSize(DataType type);

        QVariant    getValue(const QModbusDataUnit& unit) const;
        QVariant    getValue() const;

        bool setValue(QModbusDataUnit& unit, const QVariant &value) const;
        bool setValue(const QVariant &value);

        static QString dataTypeToString(DataType type);
        static DataType stringtoDataType(const QString& type, bool caseInsensitive = true);

    public:
        const int           mDeviceAddress;
        const int           mAddress;

    protected:
        DataType            mType;
        QModbusDataUnit     mUnit;


};

#endif // MODBUSDATAUNIT_H
