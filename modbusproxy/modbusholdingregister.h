/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef MODBUSHOLDINGREGISTER_H
#define MODBUSHOLDINGREGISTER_H

#include "modbusdataunit.h"

#include <QObject>
#include <QModbusDevice>
#include <QVariant>
#include <QPointer>
class QModbusReply;
class QModbusClient;
class ModbusProxy;
class ModbusTCPClient;

class ModbusHoldingRegister : public QObject
{
        Q_OBJECT

        friend class ModbusProxy;

    public:
        enum Operation:int {Read = 0, Write};
    public:
        explicit ModbusHoldingRegister(ModbusDataUnit *unit, QObject *parent = nullptr, Operation operation = Read);
        explicit ModbusHoldingRegister(ModbusDataUnit *unit, QVariant value, QObject *parent = nullptr, Operation operation = Write);
        virtual ~ModbusHoldingRegister();

        void selfDestruct();

        QModbusDevice::Error errorCode() const;
        QString errorString() const;

        QVariant value() const;
        void setValue(const QVariant &newValue);

        Operation operation() const;
        void setOperation(Operation newOperation);

    protected:
        void sendRequest(ModbusTCPClient* client);

    protected slots:
        void requestFinished();
        void writeRequestFinished();
        void requestError();

    signals:
        void registerValue(QVariant value);
        void registerValueWithSource(ModbusHoldingRegister* reg, QVariant value);

        void processingError();
        void processingErrorWithSource(ModbusHoldingRegister* reg);

        void operationDone();

    protected:
        bool                            bIsUsed;
        Operation                       mOperation;
        QPointer<ModbusDataUnit>        mUnit;
        QPointer<QModbusReply>          mReply;
        QVariant                        mValue;

};

#endif // MODBUSHOLDINGREGISTER_H
