/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef MCONTROLLER_H
#define MCONTROLLER_H

#include <QDateTime>
#include <QObject>

#include <manager/managerhost.h>
#include <manager/managersettings.h>

#include "client.h"


class ModbusProxy;
namespace ManagerInternals {

class ModeBase;

class MController : public QObject
{
        Q_OBJECT
    public:
        explicit MController(ManagerHost* host, ModbusProxy* proxy, ManagerSettings* managerSettings, QObject *parent = nullptr);

    public slots:

        void clientConnected(QVariant socketId);
        void clientDisconnected(QVariant socketId);

        void fromClient(QVariantMap input, QVariant socketId);

        void setCurrentProfile(QString profile, QDateTime onSince);

    protected:

        void welcomeNewClient(QVariant clientId);
        void switchToMode(ModeBase* newMode);
        void switchToDefaultMode();

        QVariantMap currentModeInfo() const;

    protected slots:
        void communicateCurrentModeInfo();


    protected:

        QPointer<ModbusProxy>           mProxy;
        QPointer<ManagerHost>           mHost;
        QPointer<ManagerSettings>       mSettings;
        QMap <qulonglong, Client>       mClients;
        QPointer<ModeBase>              mMode;
        QDateTime                       mLastChangeTS;

        QString                         mCurrentProfile;
        QDateTime                       mOnSince;

};

} // namespace ManagerInternals

#endif // MCONTROLLER_H
