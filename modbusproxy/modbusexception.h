/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef MODBUSEXCEPTION_H
#define MODBUSEXCEPTION_H

#include <QException>

class ModbusException : public QException
{
    public:
        enum ModbusError:int {NoError = 0,
                              SettingsError,
                              ConnectionError,
                              RequestAlreadyUsed
                             };

    public:
        explicit ModbusException(ModbusError error);
        ModbusException(const ModbusException& other);

        virtual ~ModbusException() { }

        void raise() const override { throw *this; }
        ModbusException *clone() const override { return new ModbusException(*this); }

        ModbusError error() const;

    private:

        ModbusError         mError;

};

#endif // MODBUSEXCEPTION_H
