/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef MANAGERHOST_H
#define MANAGERHOST_H

#include <QObject>
#include <QPointer>
#include <QTcpServer>
#include <QTcpSocket>

class ManagerHost : public QObject
{
        Q_OBJECT
    public:
        explicit ManagerHost(int port, QObject *parent = nullptr);

    public slots:
        void start();
        void quit();

        // Publish to all clients
        void publish(QVariantMap data);

        // Publish to all clients but one
        void publishExceptToId(QVariantMap data, QVariant socketId);

        // Publish to one specific client
        void sendToId(QVariantMap data, QVariant socketId);

    protected slots:
        void newConnectionAvailable();
        void lostClientConnection();
        void readSocket();

    signals:
        void started();
        void connected();
        void finished();

        void clientConnected(QVariant socketId);
        void clientDisconnected(QVariant socketId);
        void readFromSocket(QVariantMap input);
        void readFromSocketWithId(QVariantMap input, QVariant socketId);

    private:
        quint64                             mNextSocketId;
        int                                 mPort;
        QPointer<QTcpServer>                mTcpServer;
        QList < QPointer<QTcpSocket>>       mClients;

};

#endif // MANAGERHOST_H
