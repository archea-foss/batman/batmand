/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef MANAGERSETTINGS_H
#define MANAGERSETTINGS_H

#include <QObject>
#include <QPointer>
#include "profilesettings.h"

class ManagerSettings : public QObject
{
        Q_OBJECT

    public:

        static const QString       sManagerSettings;
        static const QString       sEnabled;
        static const QString       sPort;
        static const QString       sProfiles;

    public:
        explicit ManagerSettings(const QString& filePath, QObject *parent = nullptr);
        explicit ManagerSettings(QVariantMap map, QObject *parent = nullptr);

        QString toString(const QString& prefix = "", const QString &subprefix = "") const;

        int port() const;
        bool isEnabled() const;

        QList< QPointer<ProfileSettings>> profiles() const;
        ProfileSettings* profile(const QString& profileName) const;
        ProfileSettings *defaultProfile() const;

    signals:

    private:
        void loadFrom(QVariantMap map);

    private:

        bool                                bIsEnabled;
        int                                 mPort;
        QList< QPointer<ProfileSettings>>      mModes;
        QPointer<ProfileSettings>              mDefaultProfile;





};

#endif // MANAGERSETTINGS_H
