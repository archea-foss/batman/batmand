#ifndef MANAGERINTERNALS_SCHEDULEITEM_H
#define MANAGERINTERNALS_SCHEDULEITEM_H

#include <QDateTime>

namespace ManagerInternals {

class ScheduleItem
{
    public:

        ScheduleItem();

        QString         profile;
        QDateTime       scheduledAt;
        bool            isActive;

};

} // namespace ManagerInternals

#endif // MANAGERINTERNALS_SCHEDULEITEM_H
