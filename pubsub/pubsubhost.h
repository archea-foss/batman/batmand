/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef PUBSUBHOST_H
#define PUBSUBHOST_H

#include <QObject>
#include <QPointer>
#include <QTcpServer>
#include <QVariant>

class PubSubHost : public QObject
{
        Q_OBJECT
    public:
        explicit PubSubHost(int port, QObject *parent = nullptr);

    public slots:
        void start();
        void quit();

        void publish(QVariantMap data);

    protected slots:
        void newConnectionAvailable();

    signals:
        void started();
        void connected();
        void finished();

    private:

        int                                 mPort;
        QPointer<QTcpServer>                mTcpServer;
        QList < QPointer<QTcpSocket>>       mClients;
        QVariantMap                         mLastDataSet;

};

#endif // PUBSUBHOST_H
