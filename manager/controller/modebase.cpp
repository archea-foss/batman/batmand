/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "mcontroller.h"
#include "modebase.h"
#include "persistentmode.h"
#include "scheduledmode.h"

#include <modbusproxy/modbusproxy.h>

namespace ManagerInternals {

ModeBase::ModeBase(ModbusProxy *proxy, ManagerSettings *settings, MController *controller)
    : QObject{controller}
    , mSettings {settings}
    , mProxy {proxy}
    , mController {controller}
{

}

ModeBase::~ModeBase()
{

}

ModeBase *ModeBase::factory(QVariantMap init, ModbusProxy *proxy, ManagerSettings *settings, MController *controller)
{
    QString mode = init.value("Mode").toString();
    if ("Persistent Mode" == mode)
    {
        return new PersistentMode(init, proxy, settings, controller);
    }
    else if ("Scheduled Mode" == mode)
    {
        return new ScheduledMode(init, proxy, settings, controller);
    }

    return nullptr;

}

void ModeBase::setLastKnownProfile(QString profile, QDateTime onSince)
{
    Q_UNUSED(profile);
    Q_UNUSED(onSince);

}

} // namespace ManagerInternals
