/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "modbusholdingregister.h"
#include "modbusexception.h"
#include "modbustcpclient.h"

#include <QModbusReply>
#include <QModbusClient>

ModbusHoldingRegister::ModbusHoldingRegister(ModbusDataUnit *unit, QObject *parent, Operation operation)
    : QObject{parent}
    , bIsUsed {false}
    , mOperation {operation}
{
    if (unit)
        mUnit = unit->clone(this);

}

ModbusHoldingRegister::ModbusHoldingRegister(ModbusDataUnit *unit, QVariant value, QObject *parent, Operation operation)
    : QObject{parent}
    , bIsUsed {false}
    , mOperation {operation}
    , mValue {value}
{
    if (unit)
        mUnit = unit->clone(this);
}

ModbusHoldingRegister::~ModbusHoldingRegister()
{
    if (mReply)
        mReply->deleteLater();

    if (mUnit)
        mUnit->deleteLater();
}

void ModbusHoldingRegister::selfDestruct()
{

    connect(this, &ModbusHoldingRegister::operationDone, this, &ModbusHoldingRegister::deleteLater);
}

QModbusDevice::Error ModbusHoldingRegister::errorCode() const
{
    if (mReply)
        return mReply->error();
    else
        return QModbusDevice::NoError;
}

QString ModbusHoldingRegister::errorString() const
{
    if (mReply)
        return mReply->errorString();
    else
        return QString();
}

void ModbusHoldingRegister::requestFinished()
{

    if (mReply->result().isValid())
    {
        // qDebug() << "Value is valid, lets parse";

        if(mUnit)
        {
            mValue = mUnit->getValue(mReply->result());
            // qDebug() << "Parsed value: " << value;

            emit registerValue(mValue);
            emit registerValueWithSource(this, mValue);
            emit operationDone();

        }

    }
}

void ModbusHoldingRegister::writeRequestFinished()
{

    if (mReply->result().isValid())
    {

        emit registerValue(mValue);
        emit registerValueWithSource(this, mValue);
        emit operationDone();

    }
}

void ModbusHoldingRegister::requestError()
{
    qDebug() << "*** Request got error";
    qDebug() << "    Is finished     :" << mReply->isFinished();
    qDebug() << "    Error           :" << mReply->error();
    qDebug() << "    Error Str       : " << mReply->errorString();

    qDebug() << "    Data Is valid   :" << mReply->result().isValid();
    qDebug() << "    Reply Data Unit : " << mReply->result().values();

    emit processingError();
    emit processingErrorWithSource(this);
    emit operationDone();
}

ModbusHoldingRegister::Operation ModbusHoldingRegister::operation() const
{
    return mOperation;
}

void ModbusHoldingRegister::setOperation(Operation newOperation)
{
    mOperation = newOperation;
}

void ModbusHoldingRegister::sendRequest(ModbusTCPClient *client)
{

    if (client->state() != QModbusDevice::ConnectedState)
        qWarning() << "Modbus client not connected, failes to send request";

    if (bIsUsed)
    {
        qFatal() << "Cannot reuse a ModBusHoldingRequest and this is already used once";
        ModbusException* exception = new ModbusException(ModbusException::RequestAlreadyUsed);
        exception->raise();
    }

    bIsUsed = true;

    if (mOperation == Read)
    {
        mReply = client->sendReadRequest(mUnit->unit(), mUnit->mDeviceAddress);

        connect(mReply, &QModbusReply::finished, this, &ModbusHoldingRegister::requestFinished);
        connect(mReply, &QModbusReply::errorOccurred, this, &ModbusHoldingRegister::requestError);
    }
    else
    {
        mUnit->setValue(mValue);

        // Fixa till mUnit här så att den kan användas för att skriva med!
        mReply = client->sendWriteRequest(mUnit->unit(), mUnit->mDeviceAddress);

        connect(mReply, &QModbusReply::finished, this, &ModbusHoldingRegister::writeRequestFinished);
        connect(mReply, &QModbusReply::errorOccurred, this, &ModbusHoldingRegister::requestError);

    }
}

QVariant ModbusHoldingRegister::value() const
{
    return mValue;
}

void ModbusHoldingRegister::setValue(const QVariant &newValue)
{
    mValue = newValue;
}
