/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "sampler.h"

#include <QTime>


#include <metamodel/metameasurements.h>
#include <modbusproxy/modbusdataunit.h>
#include <modbusproxy/modbusproxy.h>


const QString Sampler::sTsTag("TimeStamp");

Sampler::Sampler(MetaMeasurements *measurements, ModbusProxy *proxy, QObject *parent)
    : QObject{parent}
    , bIsFirstRun(true)
    , mTimer {nullptr}
    , mMetaMaster {measurements}
    , mModbusProxy {proxy}
{


}

Sampler::~Sampler()
{
    // mTimer is child and will be destroyed by Qt
    // mReadRequest is a child and will be destroyed by Qt
    // mModbusClient is borrowed and will be destroyed elsewhere

}

void Sampler::start()
{
    qInfo() << "Started the Next Sampler";
    if (mMetaMaster.isNull())
    {
        qWarning() << "No meta master for the sampeler. Nothing one can do.";

        emit failed();
        return;
    }

    mTimer = new QTimer(this);
    mTimer->setTimerType(Qt::VeryCoarseTimer);
    connect(mModbusProxy.data(), &ModbusProxy::lostConnection, mTimer, &QTimer::stop);

    connect(mModbusProxy.data(), &ModbusProxy::connected, this, &Sampler::startSampling);
    connect(mModbusProxy.data(), &ModbusProxy::lostConnection, this, &Sampler::lostConnection);




    if (mModbusProxy->isConnected())
    {
        this->startSampling();
    }

}

void Sampler::quit()
{
    if (mTimer)
    {
        mTimer->stop();
        delete mTimer;
    }

    emit finished();
}


void Sampler::startSampling()
{


    // Start the first reading at even times based on the intervall.

    QTime now = QTime::currentTime();

    int minterval = mMetaMaster->timeBetweenReadings() * 1000;
    int msecondsToday = now.msecsSinceStartOfDay();
    int msecondsToNextSample = 500 + minterval - (msecondsToday % minterval);

    if (msecondsToNextSample == minterval)
        msecondsToNextSample = 0;

    mTimer->singleShot(msecondsToNextSample, this, &Sampler::firstSample);
    qDebug() << "First sample in " << msecondsToNextSample << "miliseconds at "
             << QTime::currentTime().addMSecs(msecondsToNextSample).toString(Qt::ISODateWithMs);

}

void Sampler::firstSample()
{
    // qDebug() << "First sample at " << QTime::currentTime().toString(Qt::ISODateWithMs);
    connect(mTimer, &QTimer::timeout, this, &Sampler::sample);
    mTimer->setSingleShot(false);
    mTimer->start(mMetaMaster->timeBetweenReadings() * 1000);

    sample();

}

void Sampler::sample()
{
    // qDebug() << "NextSampler is sampling at " << QDateTime::currentDateTime().toString();

    if (!mReadRequest.isNull())
    {
        qWarning() << "NextSampler::An ongoing request is already in progress." << Qt::endl
                   << "Now is " << QDateTime::currentDateTime().toString() << Qt::endl
                   // << "Request TS is " << mReadRequest->value(sTsTag) << Qt::endl
                   << "Deleteing the old one";

        if (mReadRequest)
            mReadRequest->deleteLater();
        mReadRequest = nullptr;

    }

    mReadRequest = new ModbusHoldingRegisterSet(this);
    mReadRequest->selfDestruct();

    // Insert a timestamp into the dataset.
    mReadRequest->setValue(sTsTag, QDateTime::currentDateTime());

    foreach(MetaValue* value, mMetaMaster->values())
    {
        ModbusDataUnit *unit = new ModbusDataUnit(value->deviceAddress(),
                                                  value->address(),
                                                  value->type(),
                                                  mReadRequest);            // Parent  for deletion.

        // qDebug() << "Request data: " << value->name();
        // qDebug() << "  Device Address: " << value->deviceAddress();
        // qDebug() << "  Address: " << value->address();

        mReadRequest->append(value->name(), unit, ModbusHoldingRegister::Read);

    }

    connect(mReadRequest.data(), &ModbusHoldingRegisterSet::registerValues, this, &Sampler::readData);

    mModbusProxy->send(mReadRequest);

}

