/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "profilecommandsettings.h"

#include <QVariant>
#include <modbusproxy/modbusdataunit.h>


 const QString          ProfileCommandSettings::sParameterName = "Parameter Name";
 const QString          ProfileCommandSettings::sSettingsName = "Setting Name";
 const QString          ProfileCommandSettings::sDeviceAddress = "DeviceAddress";
 const QString          ProfileCommandSettings::sAddress = "Address";
 const QString          ProfileCommandSettings::sType = "Type";
 const QString         ProfileCommandSettings::sValue = "Value";

ProfileCommandSettings::ProfileCommandSettings(QVariantMap map)
{

    mParameterName = map.value(sParameterName).toString();
    mSettingsName = map.value(sSettingsName).toString();
    mDeviceAddress = map.value(sDeviceAddress).toInt();
    mAddress = map.value(sAddress).toInt();
    mType = map.value(sType).toString();
    mValue = map.value(sValue);

}

ProfileCommandSettings::ProfileCommandSettings(const ProfileCommandSettings &other)
    :mParameterName {other.mParameterName}
    ,mSettingsName {other.mSettingsName}
    ,mDeviceAddress {other.mDeviceAddress}
    ,mAddress {other.mAddress}
    ,mType {other.mType}
    , mValue{other.mValue}
{

}

ProfileCommandSettings &ProfileCommandSettings::operator =(const ProfileCommandSettings &other)
{
    mParameterName = other.mParameterName;
    mSettingsName = other.mSettingsName;
    mDeviceAddress = other.mDeviceAddress;
    mAddress = other.mAddress;
    mType = other.mType;
    mValue = other.mValue;

    return *this;
}

ProfileCommandSettings::~ProfileCommandSettings()
{
    // Nothing to clean
}

QString ProfileCommandSettings::toString(const QString &prefix, const QString &subprefix) const
{
    Q_UNUSED(subprefix);

    QString     result;
    QTextStream str(&result);

    str << prefix << "Command:" << Qt::endl
        << prefix << subprefix << mParameterName << Qt::endl
        << prefix << subprefix << mSettingsName << Qt::endl
        << prefix << subprefix << mDeviceAddress << Qt::endl
        << prefix << subprefix << mAddress << Qt::endl
        << prefix << subprefix << mType << Qt::endl
        << prefix << subprefix << mValue.toString() << Qt::endl;

    return result;

}

ModbusDataUnit *ProfileCommandSettings::createDataUnit(QObject* parent)
{
    return new ModbusDataUnit(mDeviceAddress,
                              mAddress,
                              mType,
                              parent);

}

QString ProfileCommandSettings::operationName() const
{

    return mParameterName + "; " + mSettingsName;

}

QString ProfileCommandSettings::parameterName() const
{
    return mParameterName;
}

QString ProfileCommandSettings::settingsName() const
{
    return mSettingsName;
}

int ProfileCommandSettings::deviceAddress() const
{
    return mDeviceAddress;
}

int ProfileCommandSettings::address() const
{
    return mAddress;
}

QString ProfileCommandSettings::type() const
{
    return mType;
}

QVariant ProfileCommandSettings::value() const
{
    return mValue;
}

void ProfileCommandSettings::setValue(const QVariant &newValue)
{
    mValue = newValue;
}
