/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "persistentmode.h"

#include <modbusproxy/modbusholdingregisterset.h>
#include <modbusproxy/modbusproxy.h>

#include <manager/managersettings.h>

#include <QString>

namespace ManagerInternals {

PersistentMode::PersistentMode(QVariantMap input, ModbusProxy *proxy, ManagerSettings* settings, MController *controller)
    : ManagerInternals::ModeBase{proxy, settings, controller}
{
    mProfile = input.value("Profile").toString();

}

PersistentMode::PersistentMode(const QString &mode, ModbusProxy *proxy, ManagerSettings *settings, MController *controller)
    : ManagerInternals::ModeBase{proxy, settings, controller}
    , mProfile(mode)
{

}

PersistentMode::~PersistentMode()
{

}

void PersistentMode::run()
{

    qDebug() << "Run PersistentMode, " << mProfile;

    mTimeStamp = QDateTime::currentDateTime();

    ProfileSettings *mode = mSettings->profile(mProfile);

    if (mode)
    {
        ModbusHoldingRegisterSet* modeSet = new ModbusHoldingRegisterSet(this);
        foreach(ProfileCommandSettings cmd, mode->commands())
        {
           ModbusDataUnit* unit = cmd.createDataUnit(modeSet);
           modeSet->append(cmd.operationName(), unit, cmd.value());

        }

        modeSet->selfDestruct();


        mProxy->send(modeSet);

        emit currentProfile(mProfile, mTimeStamp);
        emit modeUpdate(this->toVariantMap());


    }

    qDebug() << "Persistent mode ran";

}

void PersistentMode::quit()
{

}

QVariantMap PersistentMode::toVariantMap() const
{
    QVariantMap result;

    result.insert("Mode", "Persistent Mode");
    result.insert("Profile", mProfile);
    result.insert("Timestamp", mTimeStamp);

    return result;

}

} // namespace ManagerInternals
