/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "metameasurements.h"

#include <QFile>
#include <QJsonDocument>
#include <QStringBuilder>
#include <QVariant>
#include <QJsonParseError>


const QString       sMeasurements("Measurements");
const QString       sSecondsBetween("Seconds Between");
const QString       sMemoryRetention("Memory Retention");
const QString       sSalt("Salt");
const QString       sValues("Values");
const QString       sTimestampName("Timestamp Name");
const QString       sPubSubHost("PubSubHost");
const QString       sEnables("Enabled");
const QString       sPort("Port");


MetaMeasurements::MetaMeasurements(QVariantMap measurement, QObject *parent)
    : QObject(parent)
{

    setup(measurement);

}

MetaMeasurements::MetaMeasurements(const QString &filePath)
{
    QFile file(filePath);
    QJsonParseError error;

    file.open(QIODeviceBase::ReadOnly);
    QJsonDocument jdoc = QJsonDocument::fromJson(file.readAll(), &error);

    if (error.error != QJsonParseError::NoError)
    {
        qFatal() << "Error when parsing the Measurement JSON" << Qt::endl
                 << "Error code: " << error.error << Qt::endl
                 << "Error message: " << error.errorString() << Qt::endl
                 << "Offset : " << error.offset;
    }

    setup(jdoc.toVariant().toHash().value(sMeasurements).toMap());

}

QString MetaMeasurements::toString(const QString &prefix) const
{

    QString result;

    QTextStream str(&result);

    str << prefix << sSecondsBetween << ": " << mTimeBetweenReadings << Qt::endl;
    str << prefix << sMemoryRetention << ": " << mMemoryRetention << Qt::endl;
    str << prefix << sSalt << ": " << mSalt << Qt::endl;
    str << prefix << sTimestampName << ": " << mTimestampName << Qt::endl;
    str << prefix << sPubSubHost << ": " << mPubSubHost.toString() << Qt::endl;
    str << prefix << sValues << ": " << Qt::endl;

    foreach(MetaValue* value, mValues)
    {
        str << prefix << "MetaValue: " << Qt::endl;
        if (value)
            str << value->toString(prefix + prefix) << Qt::endl;
        else
            str << "(value is null)" << Qt::endl;

    }


    return result;

}

void MetaMeasurements::setup(QVariantMap measurement)
{

    mTimeBetweenReadings    = measurement.value(sSecondsBetween).toInt();
    mMemoryRetention        = measurement.value(sMemoryRetention).toInt();
    mSalt                   = measurement.value(sSalt).toString();
    mTimestampName          = measurement.value(sTimestampName).toString();
    mPubSubHost             = measurement.value(sPubSubHost).toMap();

    foreach(QVariant value, measurement.value(sValues).toList())
    {
        mValues.append(new MetaValue(value.toHash()));
    }

}

QVariantMap MetaMeasurements::pubSubHost() const
{
    return mPubSubHost.toMap();
}

QString MetaMeasurements::timestampName() const
{
    return mTimestampName;
}

int MetaMeasurements::timeBetweenReadings() const
{
    return mTimeBetweenReadings;
}

int MetaMeasurements::memoryRetention() const
{
    return mMemoryRetention;
}

QString MetaMeasurements::salt() const
{
    return mSalt;
}

QList<MetaValue*> MetaMeasurements::values() const
{
    return mValues;
}

QStringList MetaMeasurements::keys() const
{

    QStringList result;

    foreach(MetaValue* value, mValues)
    {
        if (value)
            result << value->name();

    }

    return result;
}

