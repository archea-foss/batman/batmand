/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef DATALOGGER_H
#define DATALOGGER_H

#include <QDate>
#include <QDir>
#include <QFile>
#include <QObject>
#include <QPointer>

#include <metamodel/metameasurements.h>

class DataLogger : public QObject
{
        Q_OBJECT

    public:

        static const QString sSeparator;
        static const QString sNoValue;

    public:
        explicit DataLogger(MetaMeasurements* measurements, const QString& logPath, QObject *parent = nullptr);

    public slots:
        void start();
        void quit();
        void loggDataset(QVariantMap data);


    protected slots:
        void closeLogFile();
        void reopenLogfile();

    signals:

        void started();
        void finished();


    protected:
        QString metaMasterHash();
        QString logFileHeader();

    private:

        QDir                            mLogPath;
        QPointer<MetaMeasurements>      mMetaMaster;
        QDate                           mCurrentDate;
        QPointer<QFile>                 mLogFile;



};

#endif // DATALOGGER_H
