/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "daemonsettings.h"

#include <QCommandLineParser>
#include <QFile>
#include <QJsonDocument>

#include <QtDebug>

const QString       sDaemonSettings("Daemon Settings");
const QString       sMeassurements("Meassurements");
const QString       sMeassurementsIncluded("Meassurements included in this file");
const QString       sManagerSettings("Manager settings");
const QString       sManagerSettingsIncluded("Manager settings included in this file");
const QString       sStuderHostname("Studer Hostname");
const QString       sStuderPort("Studer Port");
const QString       sDataPath("Data Path");

DaemonSettings::DaemonSettings()
{

    QCommandLineParser parser;
    parser.setApplicationDescription("Daemon to interact with a Studer Next system");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption config(QStringList() << "config" << "c", QCoreApplication::translate("main", "Use <configfile> file to configure daemon settings"), "configfile");
    QCommandLineOption monitorConfig(QStringList() << "meassurements" << "m", QCoreApplication::translate("main", "Use <meassurements> file for values to monitor"), "meassurements");

    QCommandLineOption hostName(QStringList() << "hostname" << "u", QCoreApplication::translate("main", "The hostname or IP for the Studer next system"), "hostname", "studer-next");
    QCommandLineOption port(QStringList() << "port" << "p", QCoreApplication::translate("main", "The port to be used for communication with the Studer next system"), "port", "502");

    QCommandLineOption dataLogPath(QStringList() << "datapath" << "d", QCoreApplication::translate("main", "Path where data is loged to"), "datapath");

    parser.addOption(config);
    parser.addOption(monitorConfig);
    parser.addOption(hostName);
    parser.addOption(port);
    parser.addOption(dataLogPath);
    parser.process(*QCoreApplication::instance());

    mDaemonConfig = parser.value(config);

    readFromConfig();

    // Command line flags overrides those in the config file.

    if (!parser.value(monitorConfig).isEmpty())
        mMeasurementConfig = parser.value(monitorConfig);

    if (!parser.value(hostName).isEmpty())
        mStuderHostname = parser.value(hostName);

    if (!parser.value(port).isEmpty())
        mStuderPort = parser.value(port);

    if (!parser.value(dataLogPath).isEmpty())
        mDataPath = parser.value(dataLogPath);

}

QVariant DaemonSettings::daemonConfig() const
{
    return mDaemonConfig;
}

QVariant DaemonSettings::measurementConfig() const
{
    return mMeasurementConfig;
}

QVariant DaemonSettings::studerHostname() const
{
    return mStuderHostname;
}

QVariant DaemonSettings::studerPort() const
{
    return mStuderPort;
}

QVariant DaemonSettings::dataPath() const
{
    return mDataPath;
}

void DaemonSettings::readFromConfig()
{

    if (mDaemonConfig.isValid() && !mDaemonConfig.toString().isEmpty())
    {
        QString filePath = mDaemonConfig.toString();

        qDebug() << "Configfile is set to " << mDaemonConfig.toString() << ", loading the config file.";
        QFile file(filePath);
        QJsonParseError error;

        file.open(QIODeviceBase::ReadOnly);
        QJsonDocument jdoc = QJsonDocument::fromJson(file.readAll(), &error);

        if (error.error != QJsonParseError::NoError)
        {
            qFatal() << "Error when parsing the Daemon configuration file" << Qt::endl
                     << "Error code: " << error.error << Qt::endl
                     << "Error message: " << error.errorString() << Qt::endl
                     << "Offset : " << error.offset;
        }

        QVariantMap settings = jdoc.toVariant().toHash().value(sDaemonSettings).toMap();

        if (settings.value(sMeassurementsIncluded).toBool() == true)
        {
            mMeasurementConfig = mDaemonConfig;
        }
        else
        {
            mMeasurementConfig  = settings.value(sMeassurements);
        }

        if (settings.value(sManagerSettingsIncluded).toBool() == true)
        {
            mManagerConfig = mDaemonConfig;
        }
        else
        {
            mManagerConfig  = settings.value(sManagerSettings);
        }

        mStuderHostname     = settings.value(sStuderHostname);
        mStuderPort         = settings.value(sStuderPort);
        mDataPath           = settings.value(sDataPath);

    }


}

QVariant DaemonSettings::managerConfig() const
{
    return mManagerConfig;
}
