/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "datacache.h"

#include <sampler/sampler.h>

DataCache::DataCache(int retentionDays, QObject *parent)
    : QObject{parent}
    , mRetentionDays {retentionDays}
{

}

void DataCache::setup()
{

    mPurgeDate = QDate::currentDate();
    mPurgeTimer = new QTimer(this);

    // connect(mPurgeTimer.data(), &QTimer::timeout, this, &DataCache::purge);
    // mPurgeTimer->setSingleShot(false);
    // mPurgeTimer->start(8 * 60 * 60 * 1000);      // Every 8h

}

void DataCache::start()
{

    emit started();
}

void DataCache::quit()
{
    if (mPurgeTimer)
    {
        mPurgeTimer->stop();
        delete mPurgeTimer;
    }

    emit finished();
}

void DataCache::cacheDataset(QVariantMap data)
{
    mLock.lockForWrite();

    QDateTime ts = data.value(Sampler::sTsTag, QDateTime::currentDateTime()).toDateTime();
    mCahce.insert(ts, data);

    mLatestRecord = data;

    mLock.unlock();

}

void DataCache::purge(bool unconditional)
{

    qDebug() << "Purge the cahce";
    if (!unconditional && mPurgeDate >= QDate::currentDate())
    {
        qDebug() << "    Not yet time to purge";
        return;
    }

    mLock.lockForWrite();

    mPurgeDate = QDate::currentDate();

    QDate   limit = QDate::currentDate().addDays(0 - mRetentionDays);

    qDebug() << "Time to purge data";
    qDebug() << "Perge data older than " << limit;

    foreach (QDateTime ts, mCahce.keys())
    {
        if (ts.date() < limit)
        {
            qDebug() << "   Removing data for " << ts.toString(Qt::ISODate);
            mCahce.remove(ts);
        }
    }


    mLock.unlock();
}

QString DataCache::toString(const QString &prefix)
{
    mLock.lockForRead();

    QString result;
    QTextStream str(&result);

    str << prefix << "Data Cache at " << QDateTime::currentDateTime().toString() << Qt::endl;


    foreach(QDateTime ts, mCahce.keys())
    {
        str << prefix << "    TimeStamp: " << ts.toString(Qt::ISODate);
        QVariantMap data = mCahce.value(ts);
        foreach(QString dataKey, data.keys())
        {
            str << ";" << dataKey << ":" << data.value(dataKey).toString();
        }
        str << Qt::endl;
    }

    mLock.unlock();

    return result;
}

QVariantMap DataCache::getLatest()
{
    mLock.lockForRead();

    QVariantMap result = mLatestRecord;

    mLock.unlock();

    return result;


}

QVariant DataCache::getLatest(const QString &key)
{
    mLock.lockForRead();

    QVariantMap record = mLatestRecord;
    mLock.unlock();

    return record.value(key);

}
