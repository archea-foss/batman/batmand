/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "modbusproxy.h"

#include <QCoreApplication>
#include <QEvent>
#include <QMetaMethod>
#include <QtDebug>


class ProcessQueueEvent : public QEvent
{
    public:

        ProcessQueueEvent()
            : QEvent { QEvent::User }
        {}

};

ModbusProxy::ModbusProxy(const QVariant &hostname, const QVariant &port, QObject *parent)
    : QObject{parent}
    , bIsConnected { false }
    , bAreProcessing {false }
    , mHostName {hostname}
    , mTcpPort {port}
{

}

ModbusProxy::~ModbusProxy()
{

}

bool ModbusProxy::event(QEvent *event)
{
    if (event && event->type() == QEvent::User)
    {
        if (bIsConnected && !bAreProcessing)
            processQueue();

        return true;
    }
    else
    {
        return QObject::event(event);
    }
}


void ModbusProxy::start()
{

    // Setup internal signals and slots to aid calls from other threads.
    // connect(this, &StuderProxy::readDataUnitCalled, this, &StuderProxy::readDataUnit, Qt::QueuedConnection);

    mModbusClient = new ModbusTCPClient(mHostName, mTcpPort, this);
    connect(mModbusClient, &ModbusTCPClient::stateChanged, this, &ModbusProxy::modbusConnectionStatusChanged);
    mModbusClient->connectDevice();

    // Start processing the queue when connected
    connect(this, &ModbusProxy::connected, this, &ModbusProxy::processQueue);

    // Prepare the reconnect-timer.
    mReconnectTimer = new QTimer();
    mReconnectTimer->setInterval(30 * 1000);
    mReconnectTimer->stop();
    connect(mReconnectTimer.data(), &QTimer::timeout, this, &ModbusProxy::modbusConnect);
    connect(this, &ModbusProxy::lostConnection, mReconnectTimer.data(), QOverload<>::of(&QTimer::start));
    connect(this, &ModbusProxy::connected, mReconnectTimer.data(), &QTimer::stop);

    emit started();


}

void ModbusProxy::quit()
{
    bIsConnected = false;
    mModbusClient->disconnectDevice();

    emit finished();
}

// This method is ran in the callers thread.
void ModbusProxy::send(ModbusHoldingRegister *request)
{

    if (request)
    {
        mProcessingMutex.lock();

        mRequestQueue.enqueue(request);

        mProcessingMutex.unlock();

        // Trigger processing of events
        QCoreApplication::instance()->postEvent(this, new ProcessQueueEvent());

    }
}

void ModbusProxy::send(ModbusHoldingRegisterSet *set)
{
    if (set)
    {
        set->sendRequest(this);
    }
}


bool ModbusProxy::isConnected() const
{
    return bIsConnected;
}


void ModbusProxy::modbusConnectionStatusChanged(QModbusDevice::State newState)
{
    if (!bIsConnected && QModbusDevice::ConnectedState == newState)
    {
        qInfo() << "Connected to Modbus server";
        bIsConnected = true;
        emit connected();

        // Trigger processing of events
        QCoreApplication::instance()->postEvent(this, new ProcessQueueEvent());

    }
    else if (bIsConnected && QModbusDevice::ConnectedState != newState)
    {
        bIsConnected = false;
        emit lostConnection();
    }
}

void ModbusProxy::modbusConnect()
{

    if (!mModbusClient.isNull() && !mModbusClient->connectDevice())
        qWarning() << "Parser: Failed to call connectDevice";

}

void ModbusProxy::processQueue()
{


    mProcessingMutex.lock();

    if (!bIsConnected)
    {
        qWarning() << "Not connected yet, cannot process queue";

        mProcessingMutex.unlock();
        return;

    }
    if (mRequestQueue.isEmpty())
    {
        // Nothing to do, stop processing
        bAreProcessing = false;

        mProcessingMutex.unlock();
        return;
    }
    else
    {
        // There are more to process.
        bAreProcessing = true;

        ModbusHoldingRegister* request = mRequestQueue.dequeue();

        if (request)
        {
            connect(request, &ModbusHoldingRegister::operationDone, this, &ModbusProxy::processQueue);

            request->sendRequest(mModbusClient);
        }
        else
        {
            qWarning() << "Got a null-request in the queue";
        }

    }

    mProcessingMutex.unlock();

}


