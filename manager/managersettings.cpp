/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "managersettings.h"

#include <QFile>
#include <QJsonDocument>
#include <QVariant>

const QString       ManagerSettings::sManagerSettings = "Manager settings";
const QString       ManagerSettings::sEnabled = "Enabled";
const QString       ManagerSettings::sPort = "Port";
const QString       ManagerSettings::sProfiles = "Profiles";

ManagerSettings::ManagerSettings(const QString &filePath, QObject *parent)
    : QObject{parent}
    , bIsEnabled {false}
{

    QFile settingsFile(filePath);

    if (!settingsFile.open(QIODeviceBase::ReadOnly | QIODeviceBase::Text))
        qFatal() << "Failed to open settings for the Manager." << Qt::endl
                 << "Terminating";

    loadFrom(QJsonDocument::fromJson(settingsFile.readAll()).toVariant().toMap().value(sManagerSettings).toMap());

}

ManagerSettings::ManagerSettings(QVariantMap map, QObject *parent)
    : QObject{parent}
    , bIsEnabled {false}
{
    loadFrom(map);
}

QString ManagerSettings::toString(const QString &prefix, const QString &subprefix) const
{
    QString result;
    QTextStream str(&result);

    str << prefix <<"Manager setting" << Qt::endl
        << prefix << sEnabled << bIsEnabled << Qt::endl
        << prefix << sPort << mPort << Qt::endl;

    str << prefix << "Modes: " << Qt::endl;

    foreach(ProfileSettings *mode, mModes)
    {
        if (mode)
            str << mode->toString(prefix + subprefix, subprefix);
    }

    if (!mDefaultProfile.isNull())
        str << prefix << "Default Mode:" << Qt::endl
            << mDefaultProfile->toString(prefix + subprefix, subprefix) << Qt::endl;

    return result;
}

void ManagerSettings::loadFrom(QVariantMap map)
{
    if (map.isEmpty())
    {
      bIsEnabled = false;
      return;
    }

    bIsEnabled = map.value(sEnabled).toBool();
    mPort = map.value(sPort).toInt();

    foreach(QVariant mode, map.value(sProfiles).toList())
    {
        ProfileSettings *m = new ProfileSettings(mode.toMap(), this);
        mModes.append(m);

        if (m->isDefault())
            mDefaultProfile = m;

    }

}

ProfileSettings* ManagerSettings::defaultProfile() const
{
    return mDefaultProfile;
}

bool ManagerSettings::isEnabled() const
{
    return bIsEnabled;
}

QList<QPointer<ProfileSettings> > ManagerSettings::profiles() const
{
    return mModes;
}

ProfileSettings *ManagerSettings::profile(const QString &profileName) const
{
    foreach(ProfileSettings* m, mModes)
    {
        if (m && m->name() == profileName)
            return m;
    }
    return nullptr;
}

int ManagerSettings::port() const
{
    return mPort;
}
