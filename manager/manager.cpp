/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "manager.h"

#include <QMetaMethod>

#include<modbusproxy/modbusdataunit.h>

#include <QtDebug>

#include <manager/controller/mcontroller.h>

Manager::Manager(ModbusProxy *modbusProxy, const QString &settingsPath, QObject *parent)
    : QObject{parent}
    , mModbusProxy {modbusProxy}
    , mManagerSettings {settingsPath}

{

    qDebug() << "Manager settings:";
    qDebug().noquote() << mManagerSettings.toString("", "    ");


}


void Manager::start()
{

    mHost = new ManagerHost(mManagerSettings.port());

    // Just for tracing, remove this connection later.
    connect(mHost.data(), &ManagerHost::readFromSocketWithId, this, &Manager::inputFromClient);

    mHost->start();
    mController = new ManagerInternals::MController(mHost, mModbusProxy, &mManagerSettings, this);

    emit started();
}

void Manager::quit()
{
    if (mHost)
        mHost->quit();

    emit finished();
}


void Manager::valueIsWritten(QVariant data)
{
    qDebug() << "Manager:Write reply with data: " << data;
}

void Manager::commandResults(QVariantMap data)
{
    qDebug() << "Got those results:" << data;
}

void Manager::inputFromClient(QVariantMap input, QVariant clientId)
{
    qDebug() << "Manager input from client" << Qt::endl
             << "    HostId : " << clientId << Qt::endl
             << "    Data   : " << input;

}

void Manager::metaCall(const char *method)
{
    int methodIndex = this->metaObject()->indexOfMethod(method);
    QMetaMethod metaMethod = this->metaObject()->method(methodIndex);
    metaMethod.invoke(this, Qt::QueuedConnection);
}
