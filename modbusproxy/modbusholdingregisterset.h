/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef MODBUSHOLDINGREGISTERSET_H
#define MODBUSHOLDINGREGISTERSET_H

#include <QMutex>
#include <QObject>
#include "modbusholdingregister.h"

class ModbusProxy;

class ModbusHoldingRegisterSet : public QObject
{
        Q_OBJECT

        friend class ModbusProxy;

    public:
        explicit ModbusHoldingRegisterSet(QObject *parent = nullptr);
        virtual ~ModbusHoldingRegisterSet();

        void selfDestruct();

        void append(const QString &key, ModbusDataUnit *dataUnit, ModbusHoldingRegister::Operation operation = ModbusHoldingRegister::Read);
        void append(const QString &key, ModbusDataUnit *dataUnit, QVariant value, ModbusHoldingRegister::Operation operation = ModbusHoldingRegister::Write);

        QVariant value(const QString &key);
        void setValue(const QString &key, QVariant value);

    protected:
        void sendRequest(ModbusProxy *proxy);

    protected slots:
        void processingErrorWithSource(ModbusHoldingRegister* request);
        void registerValueWithSource(ModbusHoldingRegister* request, QVariant data);
        void lastRegisterProcessed();

    signals:
        void processingError();
        void registerValues(QVariantMap data);
        void operationDone();

    private:

        QList<QPointer<ModbusHoldingRegister>>          mRequests;          // Ordered list of requests
        QMap<ModbusHoldingRegister*, QString>           mRequestmap;        // For mapping requests to keys
        QVariantMap                                     mResults;           // Results
        QList<QPointer<ModbusHoldingRegister>>          mFailes;            // Requests that failed

        // QMutex                                          mTriggerLock;       // Mutex on the trigger method just in case...


};

#endif // MODBUSHOLDINGREGISTERSET_H
