/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/


#include "modbusdataunit.h"

#include <QVariant>
#include "modbusdataconverter.h"


const QStringList ModbusDataUnit::sDataType{"None", "Bool", "Int", "UInt", "Float", "Float64", "Enum", "Bitfield", "Invalid"};


ModbusDataUnit::ModbusDataUnit(int deviceAdress, int address, DataType type, QObject *parent)
    :QObject {parent}
    , mDeviceAddress(deviceAdress)
    , mAddress{address}
    , mType(type)
    , mUnit(QModbusDataUnit::HoldingRegisters,
           address,
           typeSize(mType))
{

}

ModbusDataUnit::ModbusDataUnit(int deviceAdress, int address, const QString &type, QObject *parent)
    :QObject {parent}
    , mDeviceAddress(deviceAdress)
    , mAddress{address}
    , mType(stringtoDataType(type, true))
    , mUnit(QModbusDataUnit::HoldingRegisters,
           address,
           typeSize(mType))
{

}

ModbusDataUnit::~ModbusDataUnit()
{

}

ModbusDataUnit *ModbusDataUnit::clone(QObject *parent)
{
    ModbusDataUnit* result = new ModbusDataUnit(mDeviceAddress,
                                                mAddress,
                                                mType,
                                                parent);

    return result;
}

QModbusDataUnit & ModbusDataUnit::unit()
{
    return mUnit;
}

QModbusDataUnit *ModbusDataUnit::createUnit() const
{
    return new QModbusDataUnit(QModbusDataUnit::HoldingRegisters,
                               mDeviceAddress,
                               typeSize(mType));
}

int ModbusDataUnit::typeSize(DataType type)
{
    switch(type)
    {
        case None:
            return 0;

        case Bool:
            return 1;

        case Int:
            return 2;

        case UInt:
            return 2;

        case Float:
            return 2;

        case Float64:
            return 4;

        case Enum:
            return 2;

        case Bitfield:
            return 2;

        case Invalid:
            return 0;
    }

    return 0;
}

QVariant ModbusDataUnit::getValue(const QModbusDataUnit &unit) const
{
    QVariant    null;

    if (!unit.isValid())
        return null;

    switch(mType)
    {
        case None:
            return null;

        case Bool:
            return ModbusDataConverter::parseBool(unit);

        case Int:
            return ModbusDataConverter::parseInt(unit);

        case UInt:
            return ModbusDataConverter::parseUInt(unit);;

        case Float:
            return ModbusDataConverter::parseFloat(unit);

        case Float64:
            return ModbusDataConverter::parseFloat64(unit);

        case Enum:
            return ModbusDataConverter::parseEnum(unit);

        case Bitfield:
            return ModbusDataConverter::parseBitField(unit);

        case Invalid:
            return null;
    }

    return null;
}

QVariant ModbusDataUnit::getValue() const
{
    return getValue(mUnit);

}

bool ModbusDataUnit::setValue(QModbusDataUnit &unit, const QVariant& value) const
{
    if (!value.isValid())
        return false;

    switch(mType)
    {
        case None:
            return false;

        case Bool:
            return ModbusDataConverter::setBool(unit, value);

        case Int:
            return ModbusDataConverter::setInt(unit, value);

        case UInt:
            return ModbusDataConverter::setUInt(unit, value);;

        case Float:
            return false;

        case Float64:
            return false;

        case Enum:
            return ModbusDataConverter::setEnum(unit, value);

        case Bitfield:
            return ModbusDataConverter::setBitField(unit, value);

        case Invalid:
            return false;
    }

    return false;

}

bool ModbusDataUnit::setValue(const QVariant &value)
{
    return setValue(mUnit, value);

}

QString ModbusDataUnit::dataTypeToString(DataType type)
{
    return sDataType[type];
}

ModbusDataUnit::DataType ModbusDataUnit::stringtoDataType(const QString &type, bool caseInsensitive)
{
    QString lowerType = type.toLower();

    for (int i = None; i < Invalid; i++)
    {
        if (caseInsensitive)
        {
            if (sDataType.at(i).toLower() == lowerType)
                return static_cast<DataType>(i);
        }
        else
        {
            if (sDataType.at(i) == type)
                return static_cast<DataType>(i);
        }
    }

    return Invalid;

}



