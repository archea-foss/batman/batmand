/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "modbustcpclient.h"

#include <QByteArray>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>

#include "modbusexception.h"

#include <QtDebug>

ModbusTCPClient::ModbusTCPClient(const QString &filename, QObject *parent)
    : QModbusTcpClient{parent}
    , mInitalized(false)
{

    loadSettings(filename);

    setConnectionParameter(QModbusDevice::NetworkAddressParameter, mIpAdress);
    setConnectionParameter(QModbusDevice::NetworkPortParameter, mTcpPort);

}

ModbusTCPClient::ModbusTCPClient(const QVariant& hostname, const QVariant& port, QObject *parent)
    : QModbusTcpClient{parent}
    , mInitalized {true}
    , mIpAdress {hostname}
    , mTcpPort {port}
{
    qDebug() << "Initiated the Studer TCP Modbus client";
    qDebug() << "Ip Address: " << mIpAdress;
    qDebug() << "TPC Port  : " << mTcpPort;

    setConnectionParameter(QModbusDevice::NetworkAddressParameter, mIpAdress);
    setConnectionParameter(QModbusDevice::NetworkPortParameter, mTcpPort);

}

void ModbusTCPClient::loadSettings(const QString &filename)
{
    const QString   tagIpAdress("IpAdress");
    const QString   tagTcpPort("TcpPort");

    QString settingsFileName(filename);

    QFile settingsFile(settingsFileName);
    settingsFile.open(QIODeviceBase::ReadOnly | QIODeviceBase::Text);

    if (settingsFile.error()  != QFileDevice::NoError)
    {
        qCritical() << "Error when open the Studer Modbus TCP settings file";
        qCritical() << "Error code     : " << settingsFile.error();
        qCritical() << "Error info     : " << settingsFile.errorString();

        ModbusException* exception = new ModbusException(ModbusException::SettingsError);
        exception->raise();
    }



    QJsonParseError errorInfo;

    QJsonDocument   settings = QJsonDocument::fromJson(settingsFile.readAll(), &errorInfo);

    if (errorInfo.error != QJsonParseError::NoError)
    {
        qCritical() << "Error when reading Studer Modbus TCP settings";
        qCritical() << "Error code     : " << errorInfo.error;
        qCritical() << "Error offset   : " << errorInfo.offset;

        ModbusException* exception = new ModbusException(ModbusException::SettingsError);
        exception->raise();
    }

    if (!settings.isObject())
    {
        qCritical() << "Malformed Studer Modbus TCP settings";
        ModbusException* exception = new ModbusException(ModbusException::SettingsError);
        exception->raise();
    }

    QJsonObject settingsRoot =  settings.object();

    if (!settingsRoot.contains(tagIpAdress) || !settingsRoot.contains(tagTcpPort))
    {
        qCritical() << "Studer Modbus TCP settings is missing mandatory attributes";
        ModbusException* exception = new ModbusException(ModbusException::SettingsError);
        exception->raise();
    }

    mIpAdress = settingsRoot.value(tagIpAdress).toVariant();
    mTcpPort  = settingsRoot.value(tagTcpPort).toVariant();

    qDebug() << "Initiated the Studer TCP Modbus client";
    qDebug() << "Ip Address: " << mIpAdress;
    qDebug() << "TPC Port  : " << mTcpPort;

    mInitalized = true;

}
