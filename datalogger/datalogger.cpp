/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "datalogger.h"

#include <QCryptographicHash>
#include <sampler/sampler.h>


#include <QtDebug>

const QString DataLogger::sSeparator(";");
const QString DataLogger::sNoValue("");

DataLogger::DataLogger(MetaMeasurements *measurements, const QString &logPath, QObject *parent)
    : QObject{parent}
    , mLogPath {logPath}
    , mMetaMaster {measurements}
{

}

void DataLogger::start()
{
    reopenLogfile();

    emit started();
}

void DataLogger::quit()
{
    closeLogFile();
    emit finished();
}

void DataLogger::loggDataset(QVariantMap data)
{
    if (mLogFile.isNull() || mCurrentDate != QDate::currentDate())
    {
        reopenLogfile();
    }

    if (mLogFile.isNull())
    {
        qWarning() << "Logfile is null, failed to write dataset" << Qt::endl
                   << "This dataset is ignored: " << data;

        return;
    }

    if (!mLogFile->isOpen())
    {
        qWarning() << "Logfile not open, failed to write dataset" << Qt::endl
                   << "This dataset is ignored: " << data;

        return;
    }

    if (mLogFile->error() != QFileDevice::NoError)
    {
        qWarning() << "Logfile error, failed to write dataset." << Qt::endl
                   << "Error: " << mLogFile->error() << "; " << mLogFile->errorString() << Qt::endl
                   << "This dataset is ignored: " << data;

        return;
    }


    QTextStream logfile(mLogFile);

    // Start each row with the timestamp
    QDateTime ts = data.value(Sampler::sTsTag, QDateTime::currentDateTime()).toDateTime();
    logfile << ts.toString(Qt::ISODate);

    // Log the values we are suposed to log. Ignore everything else.
    foreach(QString key, mMetaMaster->keys())
    {
        logfile << sSeparator << data.value(key, sNoValue).toString();
    }

    logfile << Qt::endl;

    // Flush the file to secure the data
    logfile.flush();

}

void DataLogger::closeLogFile()
{
    // Close currently logfile if open and reset pointer.
    if (!mLogFile.isNull())
    {
        if (mLogFile->isOpen())
            mLogFile->close();

        mLogFile->deleteLater();
        mLogFile.clear();           // Clears the QPointer
    }
}


void DataLogger::reopenLogfile()
{

    closeLogFile();

    QString fileName;
    QTextStream str(&fileName);

    mCurrentDate = QDate::currentDate();

    str << "studernext-datalog-";
    str << metaMasterHash();
    str << "-";
    str << mCurrentDate.toString("yyyyMMdd");
    str << ".csv";

    mLogFile = new QFile(mLogPath.filePath(fileName));

    bool bExists = mLogFile->exists();

    if(!mLogFile->open(QIODeviceBase::WriteOnly | QIODeviceBase::Append | QIODeviceBase::Text))
    {
        qFatal() << "Failed to create logfile. Terminating";
    }

    QTextStream logfile(mLogFile);

    if (!bExists)
    {
        logfile << logFileHeader() << Qt::endl;
    }

}

QString DataLogger::metaMasterHash()
{
    QStringList keys;
    keys.append(mMetaMaster->salt());
    keys.append(mMetaMaster->timestampName());
    keys.append(mMetaMaster->keys());


    QCryptographicHash      cryptoHash(QCryptographicHash::Sha3_512);

    cryptoHash.addData(keys.join(sSeparator).toUtf8());

    QString result = cryptoHash.result().toHex(0);

    return result.first(8);


}

QString DataLogger::logFileHeader()
{
    QStringList keys;
    keys.append(mMetaMaster->timestampName());
    keys.append(mMetaMaster->keys());

    return keys.join(sSeparator);
}
