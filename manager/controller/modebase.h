/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef MANAGERINTERNALS_MODEBASE_H
#define MANAGERINTERNALS_MODEBASE_H

#include <QDateTime>
#include <QObject>
#include <QPointer>
#include <QVariant>


class ManagerSettings;
class ModbusProxy;

namespace ManagerInternals {

class MController;

class ModeBase : public QObject
{
        Q_OBJECT
    public:
        explicit ModeBase(ModbusProxy* proxy, ManagerSettings *settings, MController *controller);
        virtual ~ModeBase();

        virtual void run() = 0;
        virtual void quit() = 0;
        virtual QVariantMap toVariantMap() const = 0;

        static ModeBase* factory(QVariantMap init, ModbusProxy *proxy, ManagerSettings *settings, ManagerInternals::MController *controller);

    public slots:
        virtual void setLastKnownProfile(QString profile, QDateTime onSince);

    signals:

        void started();
        void finished();
        void modeUpdate(QVariantMap mode);
        void currentProfile(QString profile, QDateTime onSince);

    protected:
        QPointer<ManagerSettings>       mSettings;
        QPointer<ModbusProxy>           mProxy;
        QPointer<MController>           mController;

};

} // namespace ManagerInternals

#endif // MANAGERINTERNALS_MODEBASE_H
