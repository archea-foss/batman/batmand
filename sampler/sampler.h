/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef SAMPLER_H
#define SAMPLER_H

#include <QObject>
#include <QPointer>
#include <QTimer>
#include <QVariant>

#include <modbusproxy/modbusdataunit.h>
#include <modbusproxy/modbustcpclient.h>

class MetaMeasurements;
class ModbusHoldingRegisterSet;
class ModbusProxy;

class Sampler : public QObject
{
        Q_OBJECT

    public:
        const static QString           sTsTag;

    public:
        explicit Sampler(MetaMeasurements* measurements, ModbusProxy *proxy, QObject *parent = nullptr);
        virtual ~Sampler();

    public slots:
        void start();
        void quit();

    protected slots:
        void startSampling();
        void firstSample();
        void sample();

    signals:
        void started();
        void connected();
        void lostConnection();
        void finished();
        void failed();
        void readData(QVariantMap data);



    private:

        bool                                bIsFirstRun;

        QTimer*                             mTimer;
        QPointer<MetaMeasurements>          mMetaMaster;

        QPointer<ModbusProxy>               mModbusProxy;
        QPointer<ModbusHoldingRegisterSet>  mReadRequest;

};

#endif // SAMPLER_H
