/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "managerhost.h"

#include <QJsonDocument>

#include <QtDebug>

const char*     sSocketId = "SocketID";

ManagerHost::ManagerHost(int port, QObject *parent)
    : QObject{parent}
    , mNextSocketId { 0 }
    , mPort {port}
{

}

void ManagerHost::start()
{
    mTcpServer = new QTcpServer(this);
    connect(mTcpServer.data(), &QTcpServer::newConnection, this,&ManagerHost::newConnectionAvailable);

    qDebug() << "ManagerHost listens to port " << mPort;
    bool bListen = mTcpServer->listen(QHostAddress::Any, mPort);
    qDebug() << "managerHost: Listen resturned " << bListen;

    emit started();
}

void ManagerHost::quit()
{
    foreach(QTcpSocket* client, mClients)
    {
        if (client)
        {
            qDebug() << "Closing socket to client" << client->peerAddress();
            client->close();
        }
    }

    emit finished();

}

void ManagerHost::publish(QVariantMap data)
{
    QByteArray sendData = QJsonDocument::fromVariant(data).toJson();
    sendData.append(QChar::FormFeed);

    foreach(QTcpSocket* client, mClients)
    {
        if (client && client->state() == QAbstractSocket::ConnectedState)
        {
            // qDebug() << "Write to client: " << Qt::endl
            //          << "    SocketId   : " << client->property(sSocketId) << Qt::endl
            //          << "    Data       : " << data;

            client->write(sendData);
        }
    }
}

void ManagerHost::publishExceptToId(QVariantMap data, QVariant socketId)
{
    QByteArray sendData = QJsonDocument::fromVariant(data).toJson();
    sendData.append(QChar::FormFeed);

    foreach(QTcpSocket* client, mClients)
    {
        if (client
                && client->state() == QAbstractSocket::ConnectedState
                && client->property(sSocketId) != socketId)
        {
            // qDebug() << "Write to client: " << Qt::endl
            //          << "    SocketId   : " << client->property(sSocketId) << Qt::endl
            //          << "    Data       : " << data;

            client->write(sendData);
        }
    }
}

void ManagerHost::sendToId(QVariantMap data, QVariant socketId)
{
    QByteArray sendData = QJsonDocument::fromVariant(data).toJson();
    sendData.append(QChar::FormFeed);

    foreach(QTcpSocket* client, mClients)
    {
        if (client
                && client->state() == QAbstractSocket::ConnectedState
                && client->property(sSocketId) == socketId)
        {
            // qDebug() << "Write to client: " << Qt::endl
            //          << "    SocketId   : " << client->property(sSocketId) << Qt::endl
            //          << "    Data       : " << data;

            client->write(sendData);

            return;
        }
    }
}

void ManagerHost::newConnectionAvailable()
{
    QTcpSocket *newSocket = mTcpServer->nextPendingConnection();
    while(newSocket)
    {
        QVariant socketId = mNextSocketId++;

        newSocket->setProperty(sSocketId, socketId);

        qDebug() << "New command client from " << newSocket->peerAddress() << Qt::endl
                 << "    With socketId: " << socketId;

        mClients.append(newSocket);

        connect(newSocket, &QTcpSocket::disconnected, this, &ManagerHost::lostClientConnection);
        connect(newSocket, &QTcpSocket::disconnected, newSocket, &QTcpSocket::deleteLater);
        connect(newSocket, &QTcpSocket::readyRead, this, &ManagerHost::readSocket);

        // Acknowledge the connection
        QVariantHash data;
        data.insert("Reply", "Connected");
        data.insert("ClientId", socketId);

        QVariantHash welcome;
        welcome.insert("Welcome", data);

        QByteArray sendData = QJsonDocument::fromVariant(welcome).toJson();
        sendData.append(QChar::FormFeed);
        newSocket->write(sendData);

        newSocket = mTcpServer->nextPendingConnection();

        emit clientConnected(socketId);

    }
}

void ManagerHost::lostClientConnection()
{
    QTcpSocket *socket = qobject_cast<QTcpSocket*>(sender());

    if (socket && mClients.indexOf(socket) >= 0)
    {

        QVariant socketId = socket->property(sSocketId);

        qDebug() << "ManagerHost, client disconnected" << Qt::endl
                 << "    HostId : " << socketId;

        // Remove from client list
        mClients.removeAll(socket);

        emit clientDisconnected(socketId);
    }
}

void ManagerHost::readSocket()
{

    qDebug() << "ManagerHost::readSocket";

    QTcpSocket *socket = qobject_cast<QTcpSocket*>(sender());

    if (socket)
    {

        QVariantMap input = QJsonDocument::fromJson(socket->readAll()).toVariant().toMap();

        qDebug() << "ManagerHost got input" << Qt::endl
                 << "    HostId : " << socket->property(sSocketId) << Qt::endl
                 << "    Data   : " << input;

        if (!input.isEmpty())
        {

            emit readFromSocket(input);
            emit readFromSocketWithId(input, socket->property(sSocketId).toULongLong());
        }
    }
        qDebug() << "Condition for read failed";

}
