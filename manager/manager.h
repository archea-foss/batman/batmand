/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef MANAGER_H
#define MANAGER_H

#include "managerhost.h"
#include "managersettings.h"

#include <QObject>
#include <QPointer>

#include <modbusproxy/modbusproxy.h>

namespace ManagerInternals {
class MController;
}

class Manager : public QObject
{
        Q_OBJECT
    public:
        explicit Manager(ModbusProxy* modbusProxy, const QString& settingsPath, QObject *parent = nullptr);

    public slots:
        void start();
        void quit();

    protected slots:
        void valueIsWritten(QVariant data);
        void commandResults(QVariantMap data);

        void inputFromClient(QVariantMap input, QVariant clientId);


    signals:
        void started();
        void connected();
        void finished();
        void failed();
        void lostConnection();

    private:

        void metaCall(const char *method);

    private:
        QPointer<ModbusProxy>                       mModbusProxy;       // Shared modbus proxy
        ManagerSettings                             mManagerSettings;

        QPointer<ManagerHost>                       mHost;
        QPointer<ModbusHoldingRegisterSet>          mCmdSetAux2;
        QPointer<ManagerInternals::MController>     mController;


};

#endif // MANAGER_H
