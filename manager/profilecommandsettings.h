/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef PROFILECOMMANDSETTINGS_H
#define PROFILECOMMANDSETTINGS_H

#include <QVariant>

class ModbusDataUnit;
class ProfileCommandSettings
{
    public:

        static const QString          sParameterName;
        static const QString          sSettingsName;
        static const QString          sDeviceAddress;
        static const QString          sAddress;
        static const QString          sType;
        static const QString          sValue;

    public:
        explicit ProfileCommandSettings(QVariantMap map);
        ProfileCommandSettings(const ProfileCommandSettings& other);

        ProfileCommandSettings& operator = (const ProfileCommandSettings& other);

        ~ProfileCommandSettings();
        QString toString(const QString& prefix = "", const QString &subprefix = "") const;

        ModbusDataUnit* createDataUnit(QObject *parent = nullptr);

        QString operationName() const;
        QString parameterName() const;

        QString settingsName() const;

        int deviceAddress() const;

        int address() const;

        QString type() const;

        QVariant value() const;
        void setValue(const QVariant &newValue);

    private:

        QString                 mParameterName;
        QString                 mSettingsName;
        int                     mDeviceAddress;
        int                     mAddress;
        QString                 mType;
        QVariant                mValue;

};

#endif // PROFILECOMMANDSETTINGS_H
