/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "pubsubhost.h"

#include <QJsonDocument>
#include <QTcpSocket>


PubSubHost::PubSubHost(int port, QObject *parent)
    : QObject{parent}
    , mPort {port}
{

}

void PubSubHost::start()
{

    mTcpServer = new QTcpServer(this);
    connect(mTcpServer.data(), &QTcpServer::newConnection, this,&PubSubHost::newConnectionAvailable);
    bool bListen = mTcpServer->listen(QHostAddress::Any, mPort);

    qDebug() << "PubSubHost: Listen resturned " << bListen;
    emit started();
}

void PubSubHost::quit()
{
    foreach(QTcpSocket* client, mClients)
    {
        if (client)
        {
            qDebug() << "Closing socket to client" << client->peerAddress();
            client->close();
        }
    }

    emit finished();
}

void PubSubHost::publish(QVariantMap data)
{

    mLastDataSet = data;
    QByteArray sendData = QJsonDocument::fromVariant(data).toJson();

    foreach(QTcpSocket* client, mClients)
    {
        if (client && client->state() == QAbstractSocket::ConnectedState)
        {
            // qDebug() << "write data to client" << client->peerAddress();
            client->write(sendData);
        }
    }

}

void PubSubHost::newConnectionAvailable()
{
    QTcpSocket *newSocket = mTcpServer->nextPendingConnection();
    while(newSocket)
    {
        qDebug() << "Got new conenction from " << newSocket->peerAddress();
        mClients.append(newSocket);
        connect(newSocket, &QTcpSocket::disconnected, newSocket, &QTcpSocket::deleteLater);

        // Write last known information to the new connection.
        QByteArray sendData = QJsonDocument::fromVariant(mLastDataSet).toJson();
        newSocket->write(sendData);

        newSocket = mTcpServer->nextPendingConnection();

    }
}
