/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "batmandaemon.h"

#include <QCommandLineParser>
#include <QCoreApplication>

#include <QtDebug>

#include <metamodel/metameasurements.h>
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    a.setApplicationName("studernextd");
    a.setApplicationVersion("0.0.1");

    BatManDaemon    daemon;
    daemon.setup();
    daemon.start();

   return a.exec();
}

