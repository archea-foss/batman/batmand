#ifndef MANAGERINTERNALS_SCHEDULEDMODE_H
#define MANAGERINTERNALS_SCHEDULEDMODE_H

#include "mcontroller.h"
#include "scheduleitem.h"
#include "modebase.h"

#include <modbusproxy/modbusproxy.h>
#include <manager/managersettings.h>

namespace ManagerInternals {

class ScheduledMode : public ModeBase
{
        Q_OBJECT
    public:
        explicit ScheduledMode(QVariantMap input, ModbusProxy* proxy, ManagerSettings *settings, MController *controller);
        virtual ~ScheduledMode();

        virtual void run() override;
        virtual void quit() override;

        virtual QVariantMap toVariantMap() const override;

    public slots:
        virtual void setLastKnownProfile(QString profile, QDateTime onSince) override;

    protected slots:

        void applyProfile(ScheduleItem* item);
        void applyNextProfile();
        void scheduleNextProfile();

    signals:

    private:

        ScheduleItem            *mBeforeStartProfile;

        QList<ScheduleItem*>    mSchedule;
        QTimer                  mTimer;

};

} // namespace ManagerInternals

#endif // MANAGERINTERNALS_SCHEDULEDMODE_H
