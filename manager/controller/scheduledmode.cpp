#include "scheduledmode.h"

#include <QDateTime>

#include <QtDebug>

namespace ManagerInternals {

ScheduledMode::ScheduledMode(QVariantMap input, ModbusProxy *proxy, ManagerSettings *settings, MController *controller)
    : ModeBase{proxy, settings, controller}
    , mBeforeStartProfile {nullptr}
{

    qDebug() << "Setting up a Scheduled Mode";
    qDebug() << "Input: " << input;

    foreach(QVariant base, input.value("Schedule").toList())
    {
        QVariantMap itemInfo = base.toMap();
        ScheduleItem* item = new ScheduleItem;

        item->profile = itemInfo.value("Profile").toString();
        item->scheduledAt = itemInfo.value("Scheduled At").toDateTime();

        mSchedule.append(item);
    }

    // Sort item by scheduled time in ascennding order.

    std::sort(mSchedule.begin(), mSchedule.end(),
              [](const ScheduleItem* a, const ScheduleItem* b)->bool
                {
                        return a->scheduledAt < b->scheduledAt;
                });


    // Determin the present mode if any
    // Present mode is removed from the list.

    ScheduleItem    *currentMode = nullptr;
    QDateTime       now = QDateTime::currentDateTime();

    while(!mSchedule.isEmpty() && mSchedule.first() && mSchedule.first()->scheduledAt <= now)
    {
        if (currentMode)
            delete currentMode;
        currentMode = mSchedule.takeFirst();
    }

    if (currentMode)
    {
        mSchedule.prepend(currentMode);
    }

    mTimer.callOnTimeout(this, &ScheduledMode::applyNextProfile);
    mTimer.callOnTimeout(this, &ScheduledMode::scheduleNextProfile);
}

ScheduledMode::~ScheduledMode()
{

}

void ScheduledMode::run()
{
    qDebug() << "Starts ScheduledMode";

    scheduleNextProfile();

    qDebug() << "ScheduledMode Started";

}

void ScheduledMode::quit()
{

}

QVariantMap ScheduledMode::toVariantMap() const
{
    QVariantMap result;

    result.insert("Mode", "Scheduled Mode");

    if (mBeforeStartProfile)
    {
        result.insert("Profile", mBeforeStartProfile->profile);
        result.insert("Timestamp", mBeforeStartProfile->scheduledAt);
    }

    QVariantList schedule;

    foreach(ScheduleItem *item, mSchedule)
    {
         QVariantMap itemInfo;

         itemInfo.insert("Profile", item->profile);
         itemInfo.insert("Scheduled At", item->scheduledAt);

         if (item->isActive)
         {
             itemInfo.insert("Is active", true);

             // Overwrite the current profile information.
             result.insert("Profile", item->profile);
             result.insert("Timestamp", item->scheduledAt);
         }

         schedule.append(itemInfo);
    }

    result.insert("Schedule", schedule);

    return result;
}

void ScheduledMode::setLastKnownProfile(QString profile, QDateTime onSince)
{
    if (!mBeforeStartProfile)
        mBeforeStartProfile = new ScheduleItem;

    mBeforeStartProfile->profile = profile;
    mBeforeStartProfile->scheduledAt = onSince;

}

void ScheduledMode::applyProfile(ScheduleItem* item)
{
    qDebug() << "ScheduledMode::applyProfile: " << ((item) ? item->profile : "nullpointer");

    if (item)
    {
        qDebug() << "Activating " << item->profile;

        ProfileSettings *mode = mSettings->profile(item->profile);

        if (mode)
        {
            ModbusHoldingRegisterSet* modeSet = new ModbusHoldingRegisterSet(this);
            foreach(ProfileCommandSettings cmd, mode->commands())
            {
               ModbusDataUnit* unit = cmd.createDataUnit(modeSet);
               modeSet->append(cmd.operationName(), unit, cmd.value());

            }

            modeSet->selfDestruct();
            mProxy->send(modeSet);

            item->isActive = true;

            qDebug() << "Scheduled profile activated";
        }
        else
        {
            qWarning() << "Failed to start active profile, ProfileSettings was null" << Qt::endl
                       << "Now: " << QDateTime::currentDateTime()  << Qt::endl
                       << "Scheduled for: " << item->scheduledAt;
        }

    }
    else
    {
        qWarning() << "Failed to start active profile, item was nullptr" << Qt::endl
                   << "Now: " << QDateTime::currentDateTime();
    }


}

void ScheduledMode::applyNextProfile()
{

    qDebug() << "ScheduledMode::applyNextProfile";

    // Check if the first one is active.
    if (!mSchedule.isEmpty() && mSchedule.first() && mSchedule.first()->isActive)
    {
        // The first one is active.

        // And it is the last one then stick to it.
        if (mSchedule.count() == 1)
            return;

        // And there are more to go for.
        // Remove the current one and continue to next.
        delete mSchedule.takeFirst();

    }

    // Now the first one is not active, activate
    // first if it exitst.
    if (!mSchedule.isEmpty() && mSchedule.first())
    {
        applyProfile(mSchedule.first());

        emit currentProfile(mSchedule.first()->profile, mSchedule.first()->scheduledAt);
        emit modeUpdate(this->toVariantMap());

    }

}

void ScheduledMode::scheduleNextProfile()
{
    qDebug() << "ScheduledMode::scheduleNextProfile";
    // Schedule next item
    if (!mSchedule.isEmpty() && mSchedule.first())
    {
        ScheduleItem* item = nullptr;

        // If first is active and there is a second
        if (mSchedule.first()->isActive && mSchedule.count() >= 2)
        {
            item = mSchedule.at(1);
        }

        // Or the first is not yet active
        else if (!mSchedule.first()->isActive)
        {
            item = mSchedule.first();
        }

        // In other cases there is no need to start next.
        // * First is active and there are no more to schedule
        // * First is inactive and there are seveal more in queue

        if (item)
        {
            if (item->scheduledAt < QDateTime::currentDateTime())
            {
                mTimer.start(100);          // Just some time to adjust, almost now.
            }
            else
            {
                quint64 sleepFor = QDateTime::currentDateTime().msecsTo(item->scheduledAt);
                mTimer.start(sleepFor);
            }
        }

    }

}

} // namespace ManagerInternals
