/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "mcontroller.h"
#include "persistentmode.h"
#include <QVariant>
#include <modbusproxy/modbusproxy.h>

namespace ManagerInternals {

MController::MController(ManagerHost *host, ModbusProxy *proxy, ManagerSettings *managerSettings, QObject *parent)
    : QObject{parent}
    , mProxy {proxy}
    , mHost {host}
    , mSettings {managerSettings}
{

    connect(mHost.data(), &ManagerHost::clientConnected, this, &MController::clientConnected);
    connect(mHost.data(), &ManagerHost::clientDisconnected, this, &MController::clientDisconnected);
    connect(mHost.data(), &ManagerHost::readFromSocketWithId, this,&MController::fromClient);

    switchToDefaultMode();

}

void MController::clientConnected(QVariant socketId)
{

    Client newClient;
    newClient.clientId = socketId;

    mClients.insert(socketId.toULongLong(), newClient);

    welcomeNewClient(socketId);

}

void MController::clientDisconnected(QVariant socketId)
{
    mClients.remove(socketId.toULongLong());
}

void MController::fromClient(QVariantMap input, QVariant socketId)
{
    if (input.contains("Apply Mode"))
    {
        QVariantMap mode = input.value("Apply Mode").toMap();
        switchToMode(ModeBase::factory(mode, mProxy, mSettings, this));
    }
    else
    {
        qWarning() << "Unexpected input from client" << Qt::endl
                   << "    ClientId : " << socketId.toULongLong() << Qt::endl
                   << "    Input    : " << input;
    }
}

void MController::setCurrentProfile(QString profile, QDateTime onSince)
{
    mCurrentProfile = profile;
    mOnSince = onSince;

}

void MController::welcomeNewClient(QVariant clientId)
{

    if (mHost && mSettings)
    {
        QVariantMap profileSettings;
        QVariantMap message;
        QVariantList modeList;

        foreach(ProfileSettings* mode, mSettings->profiles())
        {
            if (mode)
            {
                QVariantMap oneMode;
                oneMode.insert("Name", mode->name());
                oneMode.insert("Color", mode->color());
                oneMode.insert("Is default", mode->isDefault());

                modeList.append(oneMode);

                if (mode->isDefault())
                    message.insert("Default Profile", mode->name());
            }
        }

        message.insert("Profiles", modeList);

        profileSettings.insert("Profile Settings", message);

        mHost->sendToId(profileSettings, clientId);

        mHost->sendToId(currentModeInfo(), clientId);
    }

}

void MController::switchToMode(ModeBase *newMode)
{
    if (mMode)
    {
       mMode->quit();
       mMode->deleteLater();
       mMode.clear();
    }

    if (newMode)
    {
        mMode = newMode;

        QVariantHash info;

        mLastChangeTS = QDateTime::currentDateTime();

        mMode->setLastKnownProfile(mCurrentProfile, mOnSince);
        connect(newMode, &ModeBase::currentProfile, this, &MController::setCurrentProfile);

        mMode->run();

        mHost->publish(currentModeInfo());
        connect(newMode, &ModeBase::modeUpdate, this, &MController::communicateCurrentModeInfo);
    }

}

void MController::switchToDefaultMode()
{

    // Default is persistent mode of the default mode.
    if (mSettings && mSettings->defaultProfile())
    {
        mLastChangeTS = QDateTime::currentDateTime();
        PersistentMode *mode = new PersistentMode(mSettings->defaultProfile()->name(), mProxy, mSettings, this);
        switchToMode(mode);
    }

}

QVariantMap MController::currentModeInfo() const
{
    QVariantMap modeApplied;

    if (mMode)
    {
        modeApplied.insert("Mode Applied", mMode->toVariantMap());
    }
    else
    {
        modeApplied.insert("Mode Applied", "None");
    }

    return modeApplied;
}

void MController::communicateCurrentModeInfo()
{
    mHost->publish(currentModeInfo());
}

} // namespace ManagerInternals
