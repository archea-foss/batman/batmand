/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef DAEMONSETTINGS_H
#define DAEMONSETTINGS_H

#include <QString>
#include <QVariant>

class DaemonSettings
{
    public:
        DaemonSettings();

        QVariant daemonConfig() const;
        QVariant measurementConfig() const;
        QVariant studerHostname() const;
        QVariant studerPort() const;
        QVariant dataPath() const;
        QVariant managerConfig() const;

    protected:

        void readFromConfig();


    protected:

        QVariant        mDaemonConfig;
        QVariant        mManagerConfig;
        QVariant        mMeasurementConfig;
        QVariant        mStuderHostname;
        QVariant        mStuderPort;
        QVariant        mDataPath;


};

#endif // DAEMONSETTINGS_H
