/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef MODBUSDATACONVERTER_H
#define MODBUSDATACONVERTER_H


#include <QModbusDataUnit>
#include <QVariant>



class ModbusDataConverter
{
    public:

        static QVariant parseBool(const QModbusDataUnit& unit);
        static bool setBool(QModbusDataUnit &unit, const QVariant& value);
        static QVariant parseInt(const QModbusDataUnit& unit);
        static bool setInt(QModbusDataUnit& unit, const QVariant& value);
        static QVariant parseUInt(const QModbusDataUnit& unit);
        static bool setUInt(QModbusDataUnit& unit, const QVariant& value);
        static QVariant parseFloat(const QModbusDataUnit& unit);
        static QVariant parseFloat64(const QModbusDataUnit& unit);
        static QVariant parseEnum(const QModbusDataUnit& unit);
        static bool setEnum(QModbusDataUnit& unit, const QVariant& value);
        static QVariant parseBitField(const QModbusDataUnit& unit);
        static bool setBitField(QModbusDataUnit& unit, const QVariant& value);


};

#endif // MODBUSDATACONVERTER_H
