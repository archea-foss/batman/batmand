/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "profilesettings.h"

#include <QVariant>

const QString         ProfileSettings::sName = "Name";
const QString         ProfileSettings::sColor = "Color";
const QString         ProfileSettings::sCommands = "Commands";
const QString         ProfileSettings::sIsDefault = "Is default";

ProfileSettings::ProfileSettings(QVariantMap map, QObject *parent)
    : QObject{parent}
    , bIsDefault {false}
{
    mName = map.value(sName).toString();
    mColor = map.value(sColor).toString();
    bIsDefault = map.value(sIsDefault, QVariant(false)).toBool();

    foreach(QVariant cmd, map.value(sCommands).toList())
    {
        mCommands.append( ProfileCommandSettings(cmd.toMap()) );
    }

}

QString ProfileSettings::toString(const QString &prefix, const QString &subprefix) const
{
    QString result;
    QTextStream str(&result);


    str << prefix << "ModeSettings" << Qt::endl
        << prefix << sName << mName << Qt::endl
        << prefix << sIsDefault << bIsDefault << Qt::endl
        << prefix << sColor << mColor << Qt::endl
        << prefix << sCommands << Qt::endl;

    foreach(ProfileCommandSettings cmd, mCommands)
    {
        str << cmd.toString(prefix + subprefix, subprefix);
    }

    return result;

}

QString ProfileSettings::name() const
{
    return mName;
}

QString ProfileSettings::color() const
{
    return mColor;
}

bool ProfileSettings::isDefault() const
{
    return bIsDefault;
}

QList<ProfileCommandSettings> ProfileSettings::commands() const
{
    return mCommands;
}
