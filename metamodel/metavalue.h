/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef METAVALUE_H
#define METAVALUE_H

#include <QString>



class MetaValue
{
    public:
        MetaValue(QVariantHash hash);

        QString toString(const QString& prefix = "") const;

        QString name() const;
        QString unit() const;
        int deviceAddress() const;
        int address() const;
        QString type() const;

    protected:
        QString     mName;
        QString     mUnit;
        int         mDeviceAddress;
        int         mAddress;
        QString     mType;
};

#endif // METAVALUE_H
