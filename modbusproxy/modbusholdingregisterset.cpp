/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "modbusholdingregisterset.h"
#include "modbusproxy.h"

ModbusHoldingRegisterSet::ModbusHoldingRegisterSet(QObject *parent)
    : QObject{parent}
{

}

ModbusHoldingRegisterSet::~ModbusHoldingRegisterSet()
{
    // All ModbusHoldingRegisters have this as parent and Qt will take care.

}

void ModbusHoldingRegisterSet::selfDestruct()
{
    connect(this, &ModbusHoldingRegisterSet::operationDone, this, &ModbusHoldingRegisterSet::deleteLater);
}

void ModbusHoldingRegisterSet::append(const QString &key, ModbusDataUnit *dataUnit, ModbusHoldingRegister::Operation operation)
{
    ModbusHoldingRegister *request = new ModbusHoldingRegister(dataUnit, this, operation);

    mRequests.append(request);
    mRequestmap.insert(request, key);

    connect(request, &ModbusHoldingRegister::registerValueWithSource, this, &ModbusHoldingRegisterSet::registerValueWithSource);
    connect(request, &ModbusHoldingRegister::processingErrorWithSource, this, &ModbusHoldingRegisterSet::processingErrorWithSource);
}

void ModbusHoldingRegisterSet::append(const QString &key, ModbusDataUnit *dataUnit, QVariant value, ModbusHoldingRegister::Operation operation)
{
    ModbusHoldingRegister *request = new ModbusHoldingRegister(dataUnit, value, this, operation);

    mRequests.append(request);
    mRequestmap.insert(request, key);
    mResults.insert(key, value);

    connect(request, &ModbusHoldingRegister::registerValueWithSource, this, &ModbusHoldingRegisterSet::registerValueWithSource);
    connect(request, &ModbusHoldingRegister::processingErrorWithSource, this, &ModbusHoldingRegisterSet::processingErrorWithSource);

}

QVariant ModbusHoldingRegisterSet::value(const QString &key)
{
    return mResults.value(key);
}

void ModbusHoldingRegisterSet::setValue(const QString &key, QVariant value)
{

    // Set the value in the map for this key.
    mResults.insert(key, value);

    // Also set the register value if one exists for this key.
    foreach(ModbusHoldingRegister* reg, mRequests)
    {
        if (reg && mRequestmap.value(reg) == key)
        {
            reg->setValue(value);
        }
    }

}

void ModbusHoldingRegisterSet::sendRequest(ModbusProxy *proxy)
{
    // qDebug() << "ModbusHoldingRegisterSet::sendRequest";

    if (mRequests.isEmpty())
    {
        emit registerValues(mResults);
        emit operationDone();

    }
    else if (proxy == nullptr)
    {
        qWarning() << "ModbusHoldingRegisterSet::sendRequest with a nullptr";
        qWarning() << "Cannot send the request";
        return;
    }
    else
    {
        ModbusHoldingRegister *request = mRequests.last();

        if (request)
        {
            connect(request, &ModbusHoldingRegister::operationDone, this, &ModbusHoldingRegisterSet::lastRegisterProcessed);
        }

        foreach(request, mRequests)
        {
            proxy->send(request);
        }


    }
    // qDebug() << "ModbusHoldingRegisterSet::sendRequest Done";

}

void ModbusHoldingRegisterSet::processingErrorWithSource(ModbusHoldingRegister *request)
{
    if (request)
    {
        qWarning() << "Got an error:" << request->errorString();
        mFailes.append(request);
    }

}

void ModbusHoldingRegisterSet::registerValueWithSource(ModbusHoldingRegister *request, QVariant data)
{
    if (request)
    {
        if (mRequestmap.contains(request))
        {

            QString key = mRequestmap.value(request);

            // qDebug() << "Got data for " << key << " : " << data;
            this->setValue(key, data);
        }
        else
        {
            qWarning() << "Got data from an unknown request" << Qt::endl
                       << "    Request: " << request;
        }
    }
}

void ModbusHoldingRegisterSet::lastRegisterProcessed()
{

    // qDebug() << "ModbusHoldingRegisterSet::lastRegisterProcessed";

    if (!mFailes.isEmpty())
    {
        emit processingError();
    }

    emit registerValues(mResults);
    emit operationDone();

}


