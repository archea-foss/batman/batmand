/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef BATMANDAEMON_H
#define BATMANDAEMON_H

#include "daemonsettings.h"

#include <QObject>
#include <QPointer>
#include <QSocketNotifier>
#include <QThread>

#include <metamodel/metameasurements.h>

#include <modbusproxy/modbustcpclient.h>
#include <modbusproxy/modbusproxy.h>
#include <sampler/sampler.h>
#include <datalogger/datalogger.h>
#include <datacache/datacache.h>
#include <pubsub/pubsubhost.h>
#include <manager/manager.h>


class BatManDaemon : public QObject
{
        Q_OBJECT
    public:
        explicit BatManDaemon(QObject *parent = nullptr);

        virtual ~BatManDaemon();

        // Unix signal handlers.
        static void hupSignalHandler(int unused);
        static void termSignalHandler(int unused);
        static void user1SignalHandler(int unused);
        static int setup_unix_signal_handlers();

    public slots:

        void setup();
        void start();
        void quit();

        // Qt signal handlers.
        void handleSigHup();
        void handleSigTerm();
        void handleSigUser1();

    protected slots:
        void syncBeforeQuit();

    signals:

        void sigHup();
        void sigTerm();
        void sigUser1();
        void shutdown();

    protected:

    protected:

        DaemonSettings                          mSettings;

        QPointer<ModbusProxy>                   mModbusProxy;
        QPointer<MetaMeasurements>              mMeasurements;
        QPointer<Sampler>                   mSampler;
        QPointer<Manager>                       mManager;
        QPointer<DataLogger>                    mLogger;
        QPointer<DataCache>                     mDataCache;
        QPointer<PubSubHost>                    mMeasurementsHost;

        QPointer<QThread>                       mModbusProxyThread;
        QPointer<QThread>                       mSamplerThread;
        QPointer<QThread>                       mManagerThread;
        QPointer<QThread>                       mLoggerThread;
        QPointer<QThread>                       mMeasurementsHostThread;

    private:

        static int sighupFd[2];
        static int sigtermFd[2];
        static int sigUser1Fd[2];

        QSocketNotifier *snHup;
        QSocketNotifier *snTerm;
        QSocketNotifier *snUser1;

        int                                     mThreadsToSyncBeforeQuit;
};

#endif // BATMANDAEMON_H
