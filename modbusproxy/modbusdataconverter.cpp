/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "modbusdataconverter.h"

#include <QtDebug>

union ParseFloat
{
        struct {
                quint16 high;
                quint16 low;
        } reg;
        float value;
};

union ParseFloat64
{
        quint16 registers[4];
        qreal   value;
};

union ParseInt
{
        struct {
                quint16 high;
                quint16 low;
        } reg;
        qint32 value;
};

union ParseUInt
{
        struct {
                quint16 high;
                quint16 low;
        } reg;
        quint32 value;
};
QVariant ModbusDataConverter::parseBool(const QModbusDataUnit &unit)
{
    // qDebug() << "Parse Bool";
    // qDebug() << "Unit value size    : " << unit.values().size();

    QVariant result;

    if (unit.values().size() == 1)
    {
        // qDebug() << "Unit value         : " << unit.values().first();
        if (unit.values().first() == 0)
            result = QVariant(false);
        else
            result = QVariant(true);
    }

    return result;

}

bool ModbusDataConverter::setBool(QModbusDataUnit &unit, const QVariant &value)
{
    // qDebug() << "setBool using value " << value;
    bool bError = false;

    unit.setValueCount(1);
    QList<quint16> uvalues;
    uvalues << value.toUInt(&bError);

    unit.setValues(uvalues);

    return bError;

}

QVariant ModbusDataConverter::parseInt(const QModbusDataUnit &unit)
{
    // qDebug() << "Parse Int";
    // qDebug() << "Unit value size    : " << unit.values().size();

    QVariant result;

    if (unit.values().size() == 2)
    {
        ParseInt parser;

        parser.reg.low  = unit.values().at(0);
        parser.reg.high = unit.values().at(1);

        // qDebug() << "Parsed value: " << parser.value;

        result = QVariant(parser.value);
    }

    return result;
}

bool ModbusDataConverter::setInt(QModbusDataUnit &unit, const QVariant &value)
{
    // qDebug() << "Set Int using value " << value;

    bool bError = false;
    ParseInt parser;

    parser.value = value.toInt(&bError);

    QList<quint16> uvalues;

    uvalues << parser.reg.low;
    uvalues << parser.reg.high;

    unit.setValueCount(2);
    unit.setValues(uvalues);

    return bError;
}

QVariant ModbusDataConverter::parseUInt(const QModbusDataUnit &unit)
{
    // qDebug() << "Parse uInt";
    // qDebug() << "Unit value size    : " << unit.values().size();

    QVariant result;

    if (unit.values().size() == 2)
    {
        ParseUInt parser;

        parser.reg.low  = unit.values().at(0);
        parser.reg.high = unit.values().at(1);

        // qDebug() << "Parsed value: " << parser.value;

        result = QVariant(parser.value);
    }

    return result;
}

bool ModbusDataConverter::setUInt(QModbusDataUnit &unit, const QVariant &value)
{
    // qDebug() << "Set UInt using value " << value;

    bool bError = false;
    ParseUInt parser;

    parser.value = value.toInt(&bError);

    QList<quint16> uvalues;

    uvalues << parser.reg.low;
    uvalues << parser.reg.high;

    unit.setValueCount(2);
    unit.setValues(uvalues);

    return bError;
}

QVariant ModbusDataConverter::parseFloat(const QModbusDataUnit &unit)
{
    // qDebug() << "Parse Float";
    // qDebug() << "Unit value size    : " << unit.values().size();

    QVariant result;

    if (unit.values().size() == 2)
    {
        ParseFloat parser;

        parser.reg.low  = unit.values().at(0);
        parser.reg.high = unit.values().at(1);

        // qDebug() << "Parsed value: " << parser.value;

        result = QVariant(parser.value);
    }

    return result;

}

QVariant ModbusDataConverter::parseFloat64(const QModbusDataUnit &unit)
{
    // qDebug() << "Parse Float64";
    // qDebug() << "Unit value size    : " << unit.values().size();

    QVariant result;


    if (unit.values().size() == 4)
    {
        ParseFloat64 parser;
        parser.registers[0] = unit.value(3);
        parser.registers[1] = unit.value(2);
        parser.registers[2] = unit.value(1);
        parser.registers[3] = unit.value(0);

        // qDebug() << "Parsed value: " << parser.value;

        result = QVariant(parser.value);
    }

    return result;
}

QVariant ModbusDataConverter::parseEnum(const QModbusDataUnit &unit)
{
    // qDebug() << "Parse Enum as uInt";
    return parseUInt(unit);
}

bool ModbusDataConverter::setEnum(QModbusDataUnit &unit, const QVariant &value)
{
    // qDebug() << "Set Enum using setUInt";
    return setUInt(unit, value);

}

QVariant ModbusDataConverter::parseBitField(const QModbusDataUnit &unit)
{
    // qDebug() << "Parse Bitfield as uInt";
    return parseUInt(unit);
}

bool ModbusDataConverter::setBitField(QModbusDataUnit &unit, const QVariant &value)
{
    // qDebug() << "Set Bitfield using setUInt";
    return setUInt(unit, value);
}
