# ****************************************************************************
# Copyright 2024 Erik Ridderby,
# ARCHEA secondary business name to LIDEA AB
# 
# This file is part of BatMan Daemon - Battery Manager Daemon.
# 
# BatMan Daemon is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
# 
# BatMan Daemon is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
# 
# You should have received a copy of the GNU General Public License along
# with StuderProxy. If not, see <https://www.gnu.org/licenses/>.
# 
# ****************************************************************************

QT = core serialbus httpserver network
CONFIG += c++17 cmdline

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        batmandaemon.cpp \
        daemonsettings.cpp \
        datacache/datacache.cpp \
        datalogger/datalogger.cpp \
        main.cpp \
        manager/controller/client.cpp \
        manager/controller/mcontroller.cpp \
        manager/controller/modebase.cpp \
        manager/controller/persistentmode.cpp \
        manager/controller/scheduledmode.cpp \
        manager/controller/scheduleitem.cpp \
        manager/manager.cpp \
        manager/managerhost.cpp \
        manager/managersettings.cpp \
        manager/profilecommandsettings.cpp \
        manager/profilesettings.cpp \
        metamodel/metameasurements.cpp \
        metamodel/metavalue.cpp \
        pubsub/pubsubhost.cpp \
        restapi/apiserver.cpp \
        restapi/valueapi.cpp \
        sampler/sampler.cpp \
        modbusproxy/modbusdataconverter.cpp \
        modbusproxy/modbusdataunit.cpp \
        modbusproxy/modbusexception.cpp \
        modbusproxy/modbusholdingregister.cpp \
        modbusproxy/modbusholdingregisterset.cpp \
        modbusproxy/modbusproxy.cpp \
        modbusproxy/modbustcpclient.cpp

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    batmandaemon.h \
    daemonsettings.h \
    datacache/datacache.h \
    datalogger/datalogger.h \
    manager/controller/client.h \
    manager/controller/mcontroller.h \
    manager/controller/modebase.h \
    manager/controller/persistentmode.h \
    manager/controller/scheduledmode.h \
    manager/controller/scheduleitem.h \
    manager/manager.h \
    manager/managerhost.h \
    manager/managersettings.h \
    manager/profilecommandsettings.h \
    manager/profilesettings.h \
    metamodel/metameasurements.h \
    metamodel/metavalue.h \
    pubsub/pubsubhost.h \
    restapi/apiserver.h \
    restapi/valueapi.h \
    sampler/sampler.h \
    modbusproxy/modbusdataconverter.h \
    modbusproxy/modbusdataunit.h \
    modbusproxy/modbusexception.h \
    modbusproxy/modbusholdingregister.h \
    modbusproxy/modbusholdingregisterset.h \
    modbusproxy/modbusproxy.h \
    modbusproxy/modbustcpclient.h

LIBS += -lc

DISTFILES += \
    COPYING

