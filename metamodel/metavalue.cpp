/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#include "metavalue.h"

#include <QTextStream>
#include <QVariant>

const QString   sName("Name");
const QString   sUnit("Unit");
const QString   sDeviceAddress("DeviceAddress");
const QString   sAddress("Address");
const QString   sType("Type");

MetaValue::MetaValue(QVariantHash hash)
{
    mName           = hash.value(sName).toString();
    mUnit           = hash.value(sUnit).toString();
    mDeviceAddress  = hash.value(sDeviceAddress).toInt();
    mAddress        = hash.value(sAddress).toInt();
    mType           = hash.value(sType).toString();
}

QString MetaValue::toString(const QString &prefix) const
{
    QString result;

    QTextStream str(&result);

    str << prefix << sName << ": " << mName << Qt::endl;
    str << prefix << sUnit << ": " << mUnit<< Qt::endl;
    str << prefix << sDeviceAddress << ": " << mDeviceAddress<< Qt::endl;
    str << prefix << sAddress << ": " << mAddress<< Qt::endl;
    str << prefix << sType << ": " << mType<< Qt::endl;

    return result;

}

QString MetaValue::name() const
{
    return mName;
}

QString MetaValue::unit() const
{
    return mUnit;
}

int MetaValue::deviceAddress() const
{
    return mDeviceAddress;
}

int MetaValue::address() const
{
    return mAddress;
}

QString MetaValue::type() const
{
    return mType;
}
