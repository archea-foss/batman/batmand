/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef DATACACHE_H
#define DATACACHE_H

#include <QDateTime>
#include <QMap>
#include <QObject>
#include <QPointer>
#include <QReadWriteLock>
#include <QTimer>
#include <QVariant>

class DataCache : public QObject
{
        Q_OBJECT
    public:
        explicit DataCache(int retentionDays = 60, QObject *parent = nullptr);

        void setup();                                   // Set things up. Call after move to right thread.

    public slots:
        void start();
        void quit();
        void cacheDataset(QVariantMap data);
        void purge(bool unconditional = false);

        QString toString(const QString &prefix = "");

        QVariantMap getLatest();                        // Get the latest record or an empt QVariantMap if there is no records.
        QVariant    getLatest(const QString &key);      // Get the latest value for a key or an empt QVariant


    signals:

        void started();
        void finished();

    private:

        int                                 mRetentionDays;
        QPointer<QTimer>                    mPurgeTimer;
        QReadWriteLock                      mLock;
        QMap<QDateTime, QVariantMap>        mCahce;

        QVariantMap                         mLatestRecord;

        QDate                               mPurgeDate;                 // Date of last purge.

};

#endif // DATACACHE_H
