/****************************************************************************
Copyright 2024 Erik Ridderby,
ARCHEA secondary business name to LIDEA AB

This file is part of BatMan Daemon - Battery Manager Daemon.

BatMan Daemon is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

BatMan Daemon is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with StuderProxy. If not, see <https://www.gnu.org/licenses/>.

****************************************************************************/

#ifndef METAMEASUREMENTS_H
#define METAMEASUREMENTS_H

#include "metavalue.h"

#include <QList>
#include <QObject>
#include <QString>
#include <QVariant>


class MetaMeasurements : public QObject
{
        Q_OBJECT

    public:
        MetaMeasurements(QVariantMap measurement, QObject* parent = nullptr);
        MetaMeasurements(const QString& filePath);

        QString toString(const QString& prefix = "") const;

        int timeBetweenReadings() const;
        int memoryRetention() const;
        QString salt() const;
        QList<MetaValue *> values() const;
        QStringList keys() const;

        QString timestampName() const;
        QVariantMap pubSubHost() const;

    protected:
        void setup(QVariantMap measurement);

    protected:

        int                     mTimeBetweenReadings;       // Seconds
        int                     mMemoryRetention;           // Days

        QString                 mSalt;                      // Any valid string.
        QString                 mTimestampName;             // Name used for the TimeStamp

        QList<MetaValue*>       mValues;

        QVariant                mPubSubHost;                // About the Measurements PubSub Host

};

#endif // METAMEASUREMENTS_H
